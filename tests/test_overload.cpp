/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime.hpp"
#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>

using namespace mauve::runtime;

uint64_t total_usage(const struct rusage& usage) {
  uint64_t u_sec = usage.ru_utime.tv_sec;
  uint64_t u_micro = usage.ru_utime.tv_usec;
  uint64_t s_sec = usage.ru_stime.tv_sec;
  uint64_t s_micro = usage.ru_stime.tv_usec;

  return (u_sec + s_sec) * 1000000 + (u_micro + s_micro);
}

uint64_t burn_cpu(uint64_t microsec){
  struct rusage usage;
  getrusage(RUSAGE_THREAD, &usage);
  uint64_t init = total_usage(usage);
  uint64_t duration = 0;
  while(duration < microsec) {
    getrusage(RUSAGE_THREAD, &usage);
    duration = total_usage(usage) - init;
  }
  return duration;
}


struct S: public Shell {};
struct C: public Core<S> {
  void run() {
    std::cout << "run()" << std::endl;
    burn_cpu(sec_to_us(2));
  }
};
struct F: public FiniteStateMachine<S, C> {
  ExecState<C>& E = mk_execution("E", &C::run);
  SynchroState<C>& S = mk_synchronization("S", sec_to_ns(2));

  bool configure_hook() override {
    set_initial(E);
    set_next(E, S);
    set_next(S, E);
    return true;
  }
};

struct Archi : public Architecture {
  Component<S, C, F> & cpt = mk_component<S, C, F>("cpt");

  bool configure_hook() override {
    cpt.configure();
    return true;
  }
};

int main(int argc, char const *argv[]) {
  Archi archi;
  AbstractDeployer* deployer = mk_abstract_deployer(&archi);
  archi.configure();
  std::cout << "=> archi configured" << std::endl;
  deployer->create_tasks();
  std::cout << "=> tasks created" << std::endl;
  deployer->activate();
  std::cout << "=> tasks activated" << std::endl;
  deployer->start();
  std::cout << "=> tasks started" << std::endl;
  deployer->loop();
  std::cout << "=> finish" << std::endl;
  deployer->stop();
  std::cout << "=> stopped" << std::endl;
  return 0;
}
