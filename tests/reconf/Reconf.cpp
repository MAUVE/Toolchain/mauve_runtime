/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "Reconf.hpp"
#include "mauve_runtime/Task.hpp"

// ------------------------- Writer -------------------------

void WC::write() {
  std::cout << std::endl;
  std::cout << "Writer> core   = " << this->type_name() << std::endl;
  std::string msg = "count_" + std::to_string(count++);
  std::cout << "Writer> msg    = " << msg << std::endl;
  shell().out.write(msg);
  // Reconf
  if (count == 5) {
    std::cout << "Writer> Start Reconfiguration" << std::endl;
    archi->reader->stop();
    time_ns_t clock = archi->reader->get_time();
    archi->period->set(sec_to_ns(2));
    archi->reader->cleanup_core();
    archi->reader->replace_core<RC_reconf>();
    archi->reader->configure();
    archi->reader->activate();
    archi->reader->start(clock, nullptr);
    std::cout << "Writer> Reconfiguration Completed" << std::endl;
  }
}

// ------------------------- Reader -------------------------

void RC::read() {
  std::cout << std::endl;
  std::cout << "Reader> core   = " << this->type_name() << std::endl;
  std::cout << "Reader> status = " << shell().in.get_status() << std::endl;
  std::string msg = shell().in.read();
  std::cout << "Reader> msg    = " << msg << std::endl;
}

void RC_reconf::read() {
  std::cout << std::endl;
  std::cout << "Reader> core    = " << this->type_name() << std::endl;
  std::cout << "Reader> status  = " << shell().in.get_status() << std::endl;
  std::string msg_1 = shell().in.read();
  std::string msg_2 = shell().in.read();
  std::cout << "Reader> msg 1/2 = " << msg_1 << std::endl;
  std::cout << "Reader> msg 2/2 = " << msg_2 << std::endl;
}

// ------------------------- Architecture -------------------------

bool Archi::configure_hook() {
  std::cout << "archi configure_hook" << std::endl;

  period = mk_resource<SharedData<time_ns_t>>("period");
  period->set(sec_to_ns(1));

  buffer = mk_resource<RingBuffer<std::string>>(std::string("buffer"), static_cast<size_t>(2));

  writer = mk_component<WS, WC, WF>("writer");
  reader = mk_component<RS, RC, RF>("reader");

  writer->configure();
  reader->configure();

  writer->shell().period.connect(period);
  reader->shell().period.connect(period);

  writer->shell().out.connect(buffer);
  reader->shell().in.connect(buffer);

  writer->set_affinity(0);
  writer->set_priority(20);

  reader->set_affinity(0);
  reader->set_priority(10);

  // Trick reconf
  writer->core().archi = this;

  std::cout << "archi configured" << std::endl;

  return true;
}
