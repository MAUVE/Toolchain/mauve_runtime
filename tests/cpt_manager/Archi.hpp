/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef ARCHI_HPP
#define ARCHI_HPP

#include "Reconf.hpp"
#include "mauve/runtime.hpp"

using namespace mauve::runtime;

struct Archi: public Architecture {
  Component<WriterShell, WriterCore, WriterFSM> & writer = mk_component<WriterShell, WriterCore, WriterFSM>("writer");
  Component<ReaderShell, ReaderCore, ReaderFSM> & normal = mk_component<ReaderShell, ReaderCore, ReaderFSM>("normal");
  Component<ReaderShell, ReaderCore, ReaderFSM> & backup = mk_component<ReaderShell, ReaderCore, ReaderFSM>("backup");

  SharedData<int> & data    = mk_resource<SharedData<int>>("data", 0);
  SharedData<bool> & status = mk_resource<SharedData<bool>>("status", true);

  Component<ManagerShell, ManagerCore, ManagerFSM> & manager = mk_component<ManagerShell, ManagerCore, ManagerFSM>("manager");

  bool configure_hook() override {
    writer.shell().data.connect(data.interface().write);
    writer.shell().status.connect(status.interface().write);

    normal.shell().data.connect(data.interface().read_value);
    manager.shell().status.connect(status.interface().read);

    data.configure();
    writer.configure();
    normal.configure();
    backup.configure();
    manager.configure();

    return true;
  }
};

#endif
