#####
# Copyright 2017 ONERA
#
# This file is part of the MAUVE Runtime project.
#
# MAUVE Runtime is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# MAUVE Runtime is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
#####
cmake_minimum_required (VERSION 2.8)

# SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -O3 -std=c++11 -pthread -lrt")

#---------- ---------- ----------
#           Solvers Lib
#---------- ---------- ----------

set (RECONF_DIR ${MAUVE_DIR}/tests/cpt_manager PARENT_SCOPE)

include_directories (
  .
  ../..
)

set (RECONF_HEADERS
  Reconf.hpp
  Archi.hpp
)

set (RECONF_INLINES
)

set (RECONF_SOURCES
  Reconf.cpp
)

add_library (reconf ${RECONF_HEADERS} ${RECONF_INLINES} ${RECONF_SOURCES})
target_link_libraries (
    reconf
)

add_executable(test_manager test_manager.cpp)
target_link_libraries (test_manager mauve_runtime reconf)
