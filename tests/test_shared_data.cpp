/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime/SharedData.hpp"

using namespace mauve::runtime;

int main(int argc, char const *argv[]) {
  SharedData<int> data { "data" };
  data.make(10);
  std::cout << "created" << std::endl;

  data.shell().default_value = 100;

  data.configure();
  std::cout << "configured " << std::endl;

  // TODO: data.display();

  // TODO: std::cout << "data type: " << data.interface().data_type() << std::endl;

  std::cout << "status: " << data.interface().read_status() << std::endl;
  std::cout << "value : " << data.interface().read_value() << std::endl;
  auto res = data.interface().read();
  std::cout << "read  : " << res.status << " " << res.value << std::endl;

  data.interface().write(20);
  std::cout << "status: " << data.interface().read_status() << std::endl;
  std::cout << "value : " << data.interface().read_value() << std::endl;
  std::cout << "status: " << data.interface().read_status() << std::endl;

  data.clear();
  std::cout << "status: " << data.interface().read_status() << std::endl;
  std::cout << "value : " << data.interface().read_value() << std::endl;

  return 0;
}
