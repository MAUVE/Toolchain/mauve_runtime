/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime.hpp"

#include <unistd.h>
#include <fcntl.h>
#include <mqueue.h>
#include <string>
#include <sstream>
#include <iostream>

using namespace mauve::runtime;

struct MyShell : Shell {
  bool configure_hook() override {
    this->logger().info("hello");
    return true;
  }
};

struct MyCore : Core<MyShell> {

  int i = 0;

  bool configure_hook() override {
    this->logger().info("world");
    return true;
  }
  void run() {
    logger().debug("run: {}", i++);
  }

};

struct MyFSM : FiniteStateMachine<MyShell, MyCore> {
  ExecState<MyCore> & E    = mk_execution("E", &MyCore::run);
  SynchroState<MyCore> & W = mk_synchronization("W", sec_to_ns(1));

  bool configure_hook() override {
    set_initial(E);
    set_next(E, W);
    set_next(W, E);

    return true;
  }
};

struct Archi : public Architecture {
  Component<MyShell, MyCore, MyFSM> & cpt = mk_component<MyShell, MyCore, MyFSM>("cpt");

  bool configure_hook() override {
    cpt.configure();
    return true;
  }
};

int main(int argc, char const *argv[]) {
  std::stringstream config;
  config << "default:" << std::endl;
  config << "  level: debug" << std::endl;
  config << "  type: file" << std::endl;
  config << "  filename: mauve.log" << std::endl;
  config << "cpt:" << std::endl;
  config << "  - type: stdout" << std::endl;
  config << "    level: debug" << std::endl;
  AbstractLogger::initialize(config);
  Archi archi;
  AbstractDeployer* deployer = mk_abstract_deployer(&archi);
  archi.configure();
  deployer->create_tasks();
  deployer->activate();
  deployer->start();

  deployer->loop();

  std::cout << "=========================" << std::endl;

  deployer->stop();
  deployer->activate();
  deployer->start();

  deployer->loop();
  deployer->stop();
  return 0;
}
