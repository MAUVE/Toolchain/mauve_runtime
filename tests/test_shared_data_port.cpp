/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime/SharedData.hpp"
#include "mauve/runtime/EventPort.hpp"
#include "mauve/runtime/ReadPort.hpp"
#include "mauve/runtime/WritePort.hpp"

using namespace mauve::runtime;

int main(int argc, char const *argv[]) {

  SharedData<int> data  { "data" };
  data.make(10);

  EventPort       reset { "reset" };
  ReadPort<int>   read  { "read", 1 };
  WritePort<int>  write { "write" };

  std::cout << "reset connected: " << reset.is_connected() << std::endl;
  std::cout << "read  connected: " << read.is_connected() << std::endl;
  std::cout << "write connected: " << write.is_connected() << std::endl;

  std::cout << "read : " << read() << std::endl;

  reset.connect(data.interface().reset);
  read.connect(data.interface().read_value);
  write.connect(data.interface().write);

  std::cout << "clear connected: " << reset.is_connected() << std::endl;
  std::cout << "read  connected: " << read.is_connected() << std::endl;
  std::cout << "write connected: " << write.is_connected() << std::endl;

  std::cout << "read : " << read() << std::endl;
  write(20);
  std::cout << "read : " << read() << std::endl;
  reset();
  std::cout << "read : " << read() << std::endl;

  return 0;
}
