/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_PORT_HPP
#define MAUVE_RUNTIME_PORT_HPP

#include "AbstractPort.hpp"

#include <vector>

namespace mauve {
  namespace runtime {

    class HasPort;

    template <typename S>
    class Port: public AbstractPort {
    public:
      Port() = delete;
      Port(HasPort * container, std::string const & name);

      bool is_connected() const override final;
      bool is_connected_to(S& service) const;

      bool disconnect() override final;
      bool disconnect(S& service);

      bool connect(S& service);

      bool connect_service(Service* service) override final;
      std::vector<Service*> connected_services() const override final;
      std::size_t connections_size() const override { return services.size(); }
      Service* get_connection(int index) const override;


    protected:
      virtual ~Port() noexcept;
      Port(const Port<S> & other) = delete;

      const HasPort* container;
      std::vector<S*> services;
    };
  }
}

#include "mauve/runtime/ipp/Port.ipp"

#endif
