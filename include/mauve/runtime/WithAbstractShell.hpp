/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_WITH_ABSTRACT_SHELL_HPP
#define MAUVE_RUNTIME_WITH_ABSTRACT_SHELL_HPP

namespace mauve {
  namespace runtime {

    class Shell;

    /** Trait for objects with an abstract Shell */
    struct WithAbstractShell {
      /** Get a pointer to the shell */
      virtual Shell* get_shell     () const = 0;
      /** Check if the shell is empty */
      virtual bool is_empty_shell  () const = 0;
      /** Configure th shell*/
      virtual bool configure_shell ()       = 0;
      /** Clean up the shell */
      virtual bool cleanup_shell   ()       = 0;
    };
  }
}

#endif
