/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_ARCHITECTURE_HPP
#define MAUVE_RUNTIME_ARCHITECTURE_HPP

#include <string>
#include <vector>

#include "Configurable.hpp"
#include "WithHook.hpp"
#include "WithLogger.hpp"
#include "AbstractDeployer.hpp"

namespace mauve {
  namespace runtime {

    class AbstractComponent;
    template <typename S, typename C, typename F>
    class Component;

    class AbstractResource;
    template <typename S, typename C, typename I>
    class Resource;

    /** Architecture class */
    class Architecture
    : virtual public Configurable
    , virtual public WithHook
    , virtual public WithLogger
    {
    public:
      Architecture();
      virtual ~Architecture();

      /** Architecture type name */
      std::string type_name() const;

      /** Check if the architecture is configured */
      inline bool is_configured() const override final { return _configured; };
      /** Configure the architecture */
      bool configure()           override final;
      /** Cleanup the architecture */
      void cleanup()             override final;

      bool configure_hook() override;
      void cleanup_hook()   override;

      /** Get the set of architecture components */
      const std::vector<AbstractComponent*> get_components();

      std::size_t get_components_size() const { return components.size(); }

      AbstractComponent* get_component(int id) const;
      AbstractComponent* get_component(const std::string& name) const;

      /** Get the set of architecture resources */
      const std::vector<AbstractResource *> get_resources();

      std::size_t get_resources_size() const { return resources.size(); }

      AbstractResource* get_resource(int id) const;

      AbstractResource* get_resource(const std::string & name) const;

      int get_resource_index(const AbstractResource* resource) const;

      int get_resource_index(const std::string & name) const;

    protected:

      inline AbstractLogger& logger() const override {
        return *logger_;
      }

      // ---------- Component ----------

      template <typename COMPONENT>
      COMPONENT & mk_component(std::string const & name);

      template <typename COMPONENT, typename ...P>
      COMPONENT & mk_component(std::string const & name, P... parameters);

      template <typename COMPONENT>
      COMPONENT & mk_empty_component(std::string const & name);

      template <typename SHELL, typename CORE, typename FSM>
      Component<SHELL, CORE, FSM> & mk_component(std::string const & name);

      template <typename SHELL, typename CORE, typename FSM, typename ...P>
      Component<SHELL, CORE, FSM> & mk_component(std::string const & name, P... parameters);

      template <typename SHELL, typename CORE, typename FSM>
      Component<SHELL, CORE, FSM> & mk_empty_component(std::string const & name);

      // ---------- Resource ----------

      template <typename RESOURCE>
      RESOURCE & mk_resource(std::string const & name);

      template <typename RESOURCE, typename ...P>
      RESOURCE & mk_resource(std::string const & name, P... parameters);

      template <typename RESOURCE>
      RESOURCE & mk_empty_resource(std::string const & name);

      template <typename SHELL, typename CORE, typename INTERFACE>
      Resource<SHELL, CORE, INTERFACE> & mk_resource(std::string const & name);

      template <typename SHELL, typename CORE, typename INTERFACE, typename ...P>
      Resource<SHELL, CORE, INTERFACE> & mk_resource(std::string const & name, P... parameters);

      template <typename SHELL, typename CORE, typename INTERFACE>
      Resource<SHELL, CORE, INTERFACE> & mk_empty_resource(std::string const & name);

    private:
      bool _configured;

      AbstractLogger* logger_;

      std::vector<AbstractComponent*> components;
      std::vector<AbstractResource *> resources;

    };

  }
} /* namespace mauve */

#include "ipp/Architecture.ipp"

#endif
