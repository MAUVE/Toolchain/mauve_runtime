/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_PROPERTY_HPP
#define MAUVE_RUNTIME_PROPERTY_HPP

#include <string>

namespace mauve {
  namespace runtime {

    class HasProperty;

    /** Property types */
    enum property_type { INTEGRAL, FLOATING_POINT, STRING, OTHER };

    /** Abstract property class */
    class AbstractProperty {
    public:
      /** Constructor
       * \param container property container
       * \param name property name
       */
      AbstractProperty(HasProperty* container, std::string const & name);
      virtual ~AbstractProperty() noexcept;

      /** Get the property type */
      virtual property_type get_type() const = 0;

      /** Property name */
      const std::string name;
      /** Get the property type name */
      virtual std::string type_name() const = 0;

    protected:
      const HasProperty* container;
    };

    // ------------------------- Integral -------------------------

    /** Abstract class for a Integral property */
    class AbstractIntegralProperty: public AbstractProperty {
    public:
      /** Constructor
      * \param container property container
      * \param name property name
      */
      AbstractIntegralProperty(HasProperty* container, std::string const & name);
      /** Destructor */
      virtual ~AbstractIntegralProperty() noexcept;

      /** Get the property type */
      property_type get_type() const override;

      /** Get the property value */
      virtual long get() const = 0;
      /** Set the property value */
      virtual bool set(long value) = 0;
    };

    template <typename T>
    class IntegralProperty final : public AbstractIntegralProperty {
    public:
      IntegralProperty(HasProperty* container, std::string const & name, T init_value);
      IntegralProperty(IntegralProperty const & other) = delete;
      ~IntegralProperty() noexcept;

      std::string type_name() const override;

      T get_value() const;
      bool set_value(T value);

      long get() const override;
      bool set(long value) override;

      operator T&();
      operator T() const;
      IntegralProperty<T> & operator=(T value);
      IntegralProperty<T> & operator=(IntegralProperty<T> const & other);

    private:
      T value;
    };

    // ------------------------- Floating Point -------------------------

    /** Abstract class for a Floating point property */
    class AbstractFloatingPointProperty: public AbstractProperty {
    public:
      /** Constructor
      * \param container property container
      * \param name property name
      */
      AbstractFloatingPointProperty(HasProperty* container, std::string const & name);
      /** Destructor */
      virtual ~AbstractFloatingPointProperty() noexcept;

      /** Get the property type */
      property_type get_type() const override;

      /** Get the property value */
      virtual double get() const = 0;
      /** Set the property value */
      virtual bool set(double value) = 0;
    };

    template <typename T>
    class FloatingPointProperty final : public AbstractFloatingPointProperty {
    public:
      FloatingPointProperty(HasProperty* container, std::string const & name, T init_value);
      FloatingPointProperty(FloatingPointProperty const & other) = delete;
      ~FloatingPointProperty() noexcept;

      std::string type_name() const override;

      T get_value() const;
      bool set_value(T value);

      double get() const override;
      bool set(double value) override;

      operator T&();
      operator T() const;
      FloatingPointProperty<T> & operator=(T value);
      FloatingPointProperty<T> & operator=(FloatingPointProperty<T> const & other);

    private:
      T value;
    };

    // ------------------------- String -------------------------

    class StringProperty final : public AbstractProperty {
    public:
      StringProperty(HasProperty* container, std::string const & name, std::string init_value);
      StringProperty(StringProperty const & other) = delete;
      ~StringProperty() noexcept;

      property_type get_type() const override;

      std::string type_name() const override;

      std::string get_value() const;
      bool set_value(std::string const & value);

      operator std::string&();
      operator std::string() const;
      StringProperty & operator=(std::string const & value);
      StringProperty & operator=(StringProperty const & other);

    private:
      std::string value;
    };

    // ------------------------- Other -------------------------

    template <typename T>
    class OtherProperty final : public AbstractProperty {
    public:
      OtherProperty(HasProperty* container, std::string const & name, T init_value);
      OtherProperty(OtherProperty const & other) = delete;
      ~OtherProperty() noexcept;

      property_type get_type() const override;

      std::string type_name() const override;

      T get_value() const;
      bool set_value(T value);

      operator T&();
      operator T() const;
      OtherProperty<T> & operator=(T value);
      OtherProperty<T> & operator=(OtherProperty<T> const & other);

    private:
      T value;
    };

  }
}

#include "ipp/IntegralProperty.ipp"
#include "ipp/FloatingPointProperty.ipp"
#include "ipp/OtherProperty.ipp"


#endif
