/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_ROSJSON_SINK_HPP
#define MAUVE_RUNTIME_ROSJSON_SINK_HPP

#include <spdlog/spdlog.h>
#include <spdlog/sinks/sink.h>
#include <sys/socket.h>
#include <arpa/inet.h>

namespace mauve {
  namespace runtime {

    class rosjson_sink : public spdlog::sinks::sink {
    public:
      rosjson_sink(const std::string& server_host, int server_port);
      virtual void log(const spdlog::details::log_msg& msg) override;
      virtual void flush() override {};

    private:
      int sockfd;
      struct sockaddr_in serv;
    };

  }
}

#endif
