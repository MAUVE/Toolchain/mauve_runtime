/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_MANAGER_HPP
#define MAUVE_RUNTIME_MANAGER_HPP

#include <string>
#include <functional>
#include <map>
#include <vector>

namespace mauve {
  namespace runtime {

    template <typename ARCHI>
    class Deployer;

    template <typename ARCHI>
    class Manager final {
    public:
      Manager();
      ~Manager() noexcept;

      void save(std::string const & name, std::function<bool(Deployer<ARCHI> *)> & action);
      bool apply(Deployer<ARCHI> * deployer, std::string const & name);
      std::vector<std::string> actions_name() const;

    private:
      std::map<std::string, std::function<bool(Deployer<ARCHI> *)>> actions;
    };

  }
} // namespace

#include "ipp/Manager.ipp"

#endif
