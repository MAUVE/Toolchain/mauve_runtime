/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_STATE_IPP
#define MAUVE_RUNTIME_STATE_IPP

#include "mauve/runtime/State.hpp"
#include "mauve/runtime/Transition.hpp"
#include "mauve/runtime/AbstractFiniteStateMachine.hpp"

namespace mauve {
  namespace runtime {

  // ------------------------- Constructor -------------------------

  template <typename CORE>
  State<CORE>::State(AbstractFiniteStateMachine* container, std::string const & name)
  : AbstractState { container, name }
  , next          { this }
  {}

  // ------------------------- Destructor -------------------------

  template <typename CORE>
  State<CORE>::~State() noexcept {}

  // ------------------------- Method -------------------------

  template <typename CORE>
  void State<CORE>::set_next(State<CORE> & next) {
    this->next = &next;
  }

}}

#endif
