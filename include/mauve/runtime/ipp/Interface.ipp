/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_INTERFACE_IPP
#define MAUVE_RUNTIME_INTERFACE_IPP

#include "mauve/runtime/Interface.hpp"

namespace mauve {
  namespace runtime {

    // ------------------------- Constructor -------------------------

    template <typename SHELL, typename CORE>
    Interface<SHELL, CORE>::Interface()
    : AbstractInterface {}
    , _container        { nullptr }
    , _configured       { false }
    {
    }

    // ------------------------- Destructor -------------------------

    template <typename SHELL, typename CORE>
    Interface<SHELL, CORE>::~Interface() noexcept {
      _cleanup();
      for (Service* service: services) {
        delete service;
      }
    }

    // ------------------------- Method -------------------------

    template <typename SHELL, typename CORE>
    bool Interface<SHELL, CORE>::_configure() {
      if (is_configured()) return true;
      _configured = configure_hook();
      return _configured;
    }

    template <typename SHELL, typename CORE>
    void Interface<SHELL, CORE>::_cleanup() {
      if (is_configured()) {
        cleanup_hook();
        _configured = false;
      }
    }

    // ---------- Service ----------

    template <typename SHELL, typename CORE>
    const std::vector<Service*> Interface<SHELL, CORE>::get_services() const {
      std::vector<Service*> vect;
      for (Service* service: services) {
        vect.push_back(service);
      }
      return vect;
    }

    template <typename SHELL, typename CORE>
    Service* Interface<SHELL, CORE>::get_service(std::string const & name) const {
      for (Service* service: services) {
        if (service->name == name) {
          return service;
        }
      }
      return nullptr;
    }

    template <typename SHELL, typename CORE>
    Service* Interface<SHELL, CORE>::get_service(int index) const {
      if (index < 0 || (unsigned)index >= services.size()) return nullptr;
      return services[(unsigned)index];
    }

    template <typename SHELL, typename CORE>
    int Interface<SHELL, CORE>::get_service_index(const Service* service) const {
      for (std::size_t index = 0; index < services.size(); index++) {
        if (services[index] == service) return (signed)index;
      }
      return -1;
    }

    // ---------- Event Service ----------

    template <typename SHELL, typename CORE>
    EventService & Interface<SHELL, CORE>::mk_event_service(std::string const & name, typename EventServiceImpl<CORE>::action_t action) {
      if (get_service(name) != nullptr) {
        throw new AllreadyDefinedService(name);
      }
      EventService* service = new EventServiceImpl<CORE>(this, name, action);
      services.push_back(service);
      return *service;
    }

    // ---------- Read Service ----------

    template <typename SHELL, typename CORE>
    template <typename T>
    ReadService<T> & Interface<SHELL, CORE>::mk_read_service(std::string const & name, typename ReadServiceImpl<CORE, T>::action_t action) {
      if (get_service(name) != nullptr) {
        throw new AllreadyDefinedService(name);
      }
      ReadService<T>* service = new ReadServiceImpl<CORE, T>(this, name, action);
      services.push_back(service);
      return *service;
    }

    // ---------- Write Service ----------

    template <typename SHELL, typename CORE>
    template <typename T>
    WriteService<T> & Interface<SHELL, CORE>::mk_write_service(std::string const & name, typename WriteServiceImpl<CORE, T>::action_t action) {
      if (get_service(name) != nullptr) {
        throw new AllreadyDefinedService(name);
      }
      WriteService<T>* service = new WriteServiceImpl<CORE, T>(this, name, action);
      services.push_back(service);
      return *service;
    }

    // ---------- Call Service ----------

    template <typename SHELL, typename CORE>
    template <typename R, typename ...P>
    CallService<R, P...> & Interface<SHELL, CORE>::mk_call_service(std::string const & name, typename CallServiceImpl<CORE, R, P...>::action_t action) {
      if (get_service(name) != nullptr) {
        throw new AllreadyDefinedService(name);
      }
      CallService<R, P...>* service = new CallServiceImpl<CORE, R, P...>(this, name, action);
      services.push_back(service);
      return *service;
    }

  }
} /* namespace mauve */

#endif
