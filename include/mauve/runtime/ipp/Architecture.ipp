/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_ARCHITECTURE_IPP
#define MAUVE_RUNTIME_ARCHITECTURE_IPP

#include "mauve/runtime/Architecture.hpp"
#include "mauve/runtime/Component.hpp"
#include "mauve/runtime/Resource.hpp"

namespace mauve {
  namespace runtime {

    // ------------------------- Method -------------------------

    // ---------- Component ----------

    template <typename COMPONENT>
    COMPONENT & Architecture::mk_component(std::string const & name) {
      COMPONENT* cpt = new COMPONENT{ name };
      components.push_back(cpt);
      cpt->template make();
      return *cpt;
    }

    template <typename COMPONENT, typename ...P>
    COMPONENT & Architecture::mk_component(std::string const & name, P... parameters) {
      COMPONENT * cpt = new COMPONENT{ name };
      components.push_back(cpt);
      cpt->template make(parameters...);
      return *cpt;
    }

    template <typename COMPONENT>
    COMPONENT & Architecture::mk_empty_component(std::string const & name) {
      COMPONENT * cpt = new COMPONENT{ name };
      components.push_back(cpt);
      return *cpt;
    }

    template <typename SHELL, typename CORE, typename FSM>
    Component<SHELL, CORE, FSM> & Architecture::mk_component(std::string const & name) {
      Component<SHELL, CORE, FSM> * cpt = new Component<SHELL, CORE, FSM>{ name };
      components.push_back(cpt);
      cpt->template make<SHELL, CORE, FSM>();
      return *cpt;
    }

    template <typename SHELL, typename CORE, typename FSM, typename ...P>
    Component<SHELL, CORE, FSM> & Architecture::mk_component(std::string const & name, P... parameters) {
      Component<SHELL, CORE, FSM> * cpt = new Component<SHELL, CORE, FSM>{ name };
      components.push_back(cpt);
      cpt->template make<SHELL, CORE, FSM>(parameters...);
      return *cpt;
    }

    template <typename SHELL, typename CORE, typename FSM>
    Component<SHELL, CORE, FSM> & Architecture::mk_empty_component(std::string const & name) {
      Component<SHELL, CORE, FSM> * cpt = new Component<SHELL, CORE, FSM>{ name };
      components.push_back(cpt);
      return *cpt;
    }

    // ---------- Resource ----------

    template <typename RESOURCE>
    RESOURCE & Architecture::mk_resource(std::string const & name) {
      RESOURCE * resource = new RESOURCE{ name };
      resources.push_back(resource);
      resource->template make();
      return *resource;
    }

    template <typename RESOURCE, typename ...P>
    RESOURCE & Architecture::mk_resource(std::string const & name, P... parameters) {
      RESOURCE * resource = new RESOURCE{ name };
      resources.push_back(resource);
      resource->template make(parameters...);
      return *resource;
    }

    template <typename RESOURCE>
    RESOURCE & Architecture::mk_empty_resource(std::string const & name) {
      RESOURCE * resource = new RESOURCE{ name };
      resources.push_back(resource);
      return *resource;
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    Resource<SHELL, CORE, INTERFACE> & Architecture::mk_resource(std::string const & name) {
      Resource<SHELL, CORE, INTERFACE> * resource = new Resource<SHELL, CORE, INTERFACE>{ name };
      resources.push_back(resource);
      resource->template make<SHELL, CORE, INTERFACE>();
      return *resource;
    }

    template <typename SHELL, typename CORE, typename INTERFACE, typename ...P>
    Resource<SHELL, CORE, INTERFACE> & Architecture::mk_resource(std::string const & name, P... parameters) {
      Resource<SHELL, CORE, INTERFACE> * resource = new Resource<SHELL, CORE, INTERFACE>{ name };
      resources.push_back(resource);
      resource->template make<SHELL, CORE, INTERFACE>(parameters...);
      return *resource;
    }

    template <typename SHELL, typename CORE, typename INTERFACE>
    Resource<SHELL, CORE, INTERFACE> & Architecture::mk_empty_resource(std::string const & name) {
      Resource<SHELL, CORE, INTERFACE> * resource = new Resource<SHELL, CORE, INTERFACE>{ name };
      resources.push_back(resource);
      return *resource;
    }

  }
} /* namespace mauve */

#endif
