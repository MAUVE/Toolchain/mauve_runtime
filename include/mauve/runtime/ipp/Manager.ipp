/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_MANAGER_IPP
#define MAUVE_RUNTIME_MANAGER_IPP

#include "mauve/runtime/Manager.hpp"
#include "mauve/runtime/Deployer.hpp"
#include "mauve/runtime/Architecture.hpp"

namespace mauve {
  namespace runtime {

  // ------------------------- Constructor -------------------------

  template<typename ARCHI>
  Manager<ARCHI>::Manager()
  {}

  // ------------------------- Destructor -------------------------

  template<typename ARCHI>
  Manager<ARCHI>::~Manager() noexcept {}

  // ------------------------- Methods -------------------------

  template<typename ARCHI>
  void Manager<ARCHI>::save(std::string const & name, std::function<bool(Deployer<ARCHI> *)> & action) {
    actions[name] = action;
  }

  template<typename ARCHI>
  bool Manager<ARCHI>::apply(Deployer<ARCHI> * deployer, std::string const & name) {
    if (deployer == nullptr)
      return false;

    auto it = actions.find(name);
    if (it == actions.end()) {
      return false;
    }

    return it->second(deployer);
  }

  template<typename ARCHI>
  std::vector<std::string> Manager<ARCHI>::actions_name() const {
    std::vector<std::string> names;
    for (auto tuple : actions) names.push_back(tuple.first);
    return names;
  }

}}

#endif
