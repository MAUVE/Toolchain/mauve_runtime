/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_HAS_CORE_IPP
#define MAUVE_RUNTIME_HAS_CORE_IPP

#include "mauve/runtime/HasCore.hpp"
#include "mauve/runtime/Core.hpp"

namespace mauve {
  namespace runtime {
    // ------------------------- Constructor -------------------------

    template <typename CORE>
    HasCore<CORE>::HasCore()
    : _core { nullptr } {}

    // ------------------------- Destructor -------------------------

    template <typename CORE>
    HasCore<CORE>::~HasCore() noexcept {
      delete _core;
    }

    // ------------------------- Methods -------------------------

    template <typename CORE>
    AbstractCore* HasCore<CORE>::get_core() const {
      return _core;
    }

    template <typename CORE>
    CORE& HasCore<CORE>::core() const {
      return *_core;
    }

    template <typename CORE>
    bool HasCore<CORE>::clear_core() {
      if (_core != nullptr && _core->is_configured()) {
        return false;
      }
      delete _core;
      return true;
    }

    template <typename CORE>
    template <typename C>
    bool HasCore<CORE>::create_core() {
      if (_core != nullptr) {
        return false;
      }
      _core = new C();
      return true;
    }

  }
}

#endif
