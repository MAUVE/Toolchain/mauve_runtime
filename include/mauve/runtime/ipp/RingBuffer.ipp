/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_RING_BUFFER_IPP
#define MAUVE_RUNTIME_RING_BUFFER_IPP

#include "mauve/runtime/RingBuffer.hpp"
#include "mauve/runtime/ReadPort.hpp"
#include "mauve/runtime/WritePort.hpp"

namespace mauve {
  namespace runtime {

    // ========================= Shell =========================

    template <typename T>
    RingBufferShell<T>::RingBufferShell(size_t size, T value)
    : Shell()
    , size { mk_property("buffer_size", size) }
    , default_value { mk_property("default_value", value) }
    {
    }

  // ========================= Core =========================

  template <typename T>
  RingBufferCore<T>::RingBufferCore()
  : Core<RingBufferShell<T>> {}
  , begin         { 0 }
  , end           { 0 }
  , status        { DataStatus::NO_DATA }
  , mutex         {}
  {
  }

  template <typename T>
  bool RingBufferCore<T>::configure_hook()
  {
    default_value = this->shell().default_value.get_value();
    size = this->shell().size.get_value();
    values = new T[size];
    for (int i = 0; i < size; i++)
    {
      values[i] = default_value;
    }
    return true;
  }

  template <typename T>
  void RingBufferCore<T>::cleanup_hook()
  {
    delete[] values;
  }

  // ---------- Reader ----------

  template <typename T>
  DataStatus RingBufferCore<T>::read_status() {
    mutex.lock();
    DataStatus res = status;
    mutex.unlock();
    return res;
  }

  template <typename T>
  size_t RingBufferCore<T>::read_length() {
      mutex.lock();
      size_t length = 0;
      if (status == DataStatus::NEW_DATA)
        length = end-begin;
      mutex.unlock();
      return length;
    }


  template <typename T>
  StatusValue<T> RingBufferCore<T>::read() {
    mutex.lock();
    DataStatus status = this->status;
    T value;
    if (this->status == DataStatus::NO_DATA) {
      value = default_value;
    }
    else if (begin == end) {
      this->status = DataStatus::OLD_DATA;
      value = values[begin];
    }
    else {
      size_t const prev = begin;
      begin = (begin + 1) % size;
      value = values[prev];
    }
    mutex.unlock();
    return StatusValue<T>{ status, value };
  }

  template <typename T>
  T RingBufferCore<T>::read_value() {
    mutex.lock();
    T value;
    if (this->status == DataStatus::NO_DATA) {
      value = default_value;
    }
    else if (begin == end) {
      this->status = DataStatus::OLD_DATA;
      value = values[begin];
    }
    else {
      size_t const prev = begin;
      begin = (begin + 1) % size;
      value = values[prev];
    }
    mutex.unlock();
    return value;
  }

  // ---------- Writer ----------

  template <typename T>
  void RingBufferCore<T>::write(T value) {
    mutex.lock();
    if (status != DataStatus::NEW_DATA) {
      values[end] = value;
    }
    else {
      end = (end + 1) % size;
      if (end == begin) {
        begin = (begin + 1) % size;
      }
      values[end] = value;
    }
    status = DataStatus::NEW_DATA;
    mutex.unlock();
  }

  // ---------- Reset ----------

  template <typename T>
  void RingBufferCore<T>::reset() {
    mutex.lock();
    status = DataStatus::NO_DATA;
    begin = end = 0;
    for (int i = 0; i < size; i++) {
      values[i] = default_value;
    }
    mutex.unlock();
  }

  // ========================= Interface =========================

  // ========================= Resource =========================

  /*
  template <typename T>
  std::string RingBuffer<T>::type_name() const {
    int status = -4;
    return abi::__cxa_demangle(typeid(T).name(), nullptr, nullptr, &status);
  }

  template <typename T>
  std::string RingBuffer<T>::display() const {
    if (status == DataStatus::NO_DATA) {
      return "[]";
    }
    std::ostringstream os;
    os << "[";
    size_t index = begin;
    os <<  values[index];
    while (index != end) {
      index = (index+1) % size;
      os << ", " << values[index];
    }
    return os.str();
  }
  */

}} /* namespace mauve */

#endif
