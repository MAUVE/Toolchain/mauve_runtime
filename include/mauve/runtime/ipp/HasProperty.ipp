/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_HAS_PROPERTY_IPP
#define MAUVE_RUNTIME_HAS_PROPERTY_IPP

#include "mauve/runtime/HasProperty.hpp"
#include "mauve/runtime/Property.hpp"

namespace mauve {
  namespace runtime {

    template <typename T>
    Property<T> & HasProperty::mk_property(std::string const & name, T init_value) {
      if (get_property(name) != nullptr) {
        throw new AlreadyDefinedProperty(name);
      }
      Property<T>* property = new Property<T>(this, name, init_value);
      properties.push_back(property);
      return *property;
    }

  }
}

#endif
