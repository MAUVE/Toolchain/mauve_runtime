/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_HAS_PORT_IPP
#define MAUVE_RUNTIME_HAS_PORT_IPP

#include "mauve/runtime/HasPort.hpp"
#include "mauve/runtime/ReadPort.hpp"
#include "mauve/runtime/WritePort.hpp"
#include "mauve/runtime/CallPort.hpp"

namespace mauve {
  namespace runtime {

    template <typename PORT, typename ... PARAM>
    PORT & HasPort::mk_port(std::string const & name, PARAM ... parameters) {
      if (get_port(name) != nullptr) {
        throw new AlreadyDefinedPort(name);
      }
      PORT * port = new PORT{ this, name, parameters ...};
      ports.push_back(port);
      return *port;
    }

    template <typename T>
    ReadPort<T> & HasPort::mk_read_port(std::string const & name, T default_value) {
      if (get_port(name) != nullptr) {
        throw new AlreadyDefinedPort(name);
      }
      ReadPort<T> * port = new ReadPort<T>{ this, name, default_value };
      ports.push_back(port);
      return *port;
    }

    template <typename T>
    WritePort<T> & HasPort::mk_write_port(std::string const & name) {
      if (get_port(name) != nullptr) {
        throw new AlreadyDefinedPort(name);
      }
      WritePort<T> * port = new WritePort<T>{ this, name };
      ports.push_back(port);
      return *port;
    }

    template <typename R, typename ...P>
    CallPort<R, P...> HasPort::mk_call_port(std::string const & name, R default_value) {
      if (get_port(name) != nullptr) {
        throw new AlreadyDefinedPort(name);
      }
      CallPort<R, P...> * port = new CallPort<R, P...>{ this, name, default_value };
      ports.push_back(port);
      return *port;
    }

  }
}

#endif
