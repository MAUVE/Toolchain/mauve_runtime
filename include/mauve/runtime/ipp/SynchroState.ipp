/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_SYNCHRO_SATE_IPP
#define MAUVE_RUNTIME_SYNCHRO_SATE_IPP

#include "mauve/runtime/SynchroState.hpp"
#include "mauve/runtime/AbstractFiniteStateMachine.hpp"

namespace mauve {
  namespace runtime {

  // ------------------------- Constructor -------------------------

  template <typename CORE>
  SynchroState<CORE>::SynchroState(AbstractFiniteStateMachine* container, std::string const & name, time_ns_t clock)
  : State<CORE> { container, name }
  , clock       { clock }
  {}

  // ------------------------- Destructor -------------------------

  template <typename CORE>
  SynchroState<CORE>::~SynchroState() noexcept {}

  // ------------------------- Operator -------------------------

  // ------------------------- Method -------------------------

  template <typename CORE>
  bool SynchroState<CORE>::is_execution() const {
    return false;
  }

  template <typename CORE>
  bool SynchroState<CORE>::is_synchronization() const {
    return true;
  }

  template <typename CORE>
  time_ns_t SynchroState<CORE>::get_clock() const {
    return clock;
  }

  template <typename CORE>
  bool SynchroState<CORE>::set_clock(time_ns_t clock) {
    if (this->container->is_configured()) {
      return false;
    }
    this->clock = clock;
    return true;
  }

  template <typename CORE>
  AbstractState* SynchroState<CORE>::get_next_state(int index) const {
    if (index != 0) return nullptr;
    return this->next;
  }

  template <typename CORE>
  std::vector<State<CORE>*> SynchroState<CORE>::next_states() const {
    std::vector<State<CORE>*> res;
    res.push_back(this->next);
    return res;
  }

  template <typename CORE>
  State<CORE> * SynchroState<CORE>::run(CORE * core) {
    core->logger().trace("running state {}", this->to_string());
    core->logger().trace("go to state {}", this->next->to_string());
    return this->next;
  }

  template <typename CORE>
  std::string SynchroState<CORE>::to_string() const {
    return "SynchroState[" + this->name + ", " + std::to_string(clock) + "]";
  }

}}

#endif
