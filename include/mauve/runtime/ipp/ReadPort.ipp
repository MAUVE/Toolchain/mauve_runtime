/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_READ_PORT_IPP
#define MAUVE_RUNTIME_READ_PORT_IPP

#include "mauve/runtime/ReadPort.hpp"
#include "mauve/runtime/ReadService.hpp"
#include <typeinfo>
#include <cxxabi.h>

namespace mauve {
  namespace runtime {

  // -------------------- Constructor --------------------

  template <typename T>
  ReadPort<T>::ReadPort(HasPort* container, std::string const & name, T default_value)
  : Port<ReadService<T>> { container, name }
  , default_value        { default_value }
  {}

  // -------------------- Destructor --------------------

  template <typename T>
  ReadPort<T>::~ReadPort() noexcept {}

  // -------------------- Method --------------------

  template <typename T>
  std::string ReadPort<T>::type_name() const {
    int status = -4;
    std::string type_name = abi::__cxa_demangle(typeid(T).name(), nullptr, nullptr, &status);
    return type_name;
  }

  template <typename T>
  T ReadPort<T>::read() const {
    T res = default_value;
    for (ReadService<T>* service: this->services) {
      res = service->read();
    }
    return res;
  }

}} /* namespace mauve */

#endif
