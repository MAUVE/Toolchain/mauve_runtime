/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_CALL_PORT_IPP
#define MAUVE_RUNTIME_CALL_PORT_IPP

#include "mauve/runtime/CallPort.hpp"
#include "mauve/runtime/CallService.hpp"
#include <typeinfo>
#include <cxxabi.h>

namespace mauve {
  namespace runtime {

  // -------------------- Constructor --------------------

  template <typename R, typename ...P>
  CallPort<R, P...>::CallPort(HasPort* container, std::string const & name, R default_value)
  : Port<CallService<R, P...>> { container, name }
  , default_value              { default_value }
  {}

  // -------------------- Destructor --------------------

  template <typename R, typename ...P>
  CallPort<R, P...>::~CallPort() noexcept {}

  // -------------------- Method --------------------

  template <typename R, typename ...P>
  std::string CallPort<R, P...>::type_name() const {
    int status = -4;
    std::string type_name = abi::__cxa_demangle(typeid(R(P...)).name(), nullptr, nullptr, &status);
    return type_name;
  }

  template <typename R, typename ...P>
  R CallPort<R, P...>::call(P... parameters) const {
    R res = default_value;
    for (CallService<R, P...>* service: this->services) {
      res = service->call(parameters...);
    }
    return res;
  }

}}

#endif
