/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_TRANSITION_HPP
#define MAUVE_RUNTIME_TRANSITION_HPP

#include <functional>
#include "AbstractState.hpp"

namespace mauve {
  namespace runtime {

  template <typename T>
  class State;

  template <typename T>
  class ExecState;

  template <typename CORE>
  class Transition {

  public:
    using Guard_t  = typename ExecState<CORE>::Guard_t;

    friend class ExecState<CORE>;

    Transition() = delete;
    Transition(Transition<CORE> const & other) = delete;
    Transition<CORE> & operator=(Transition<CORE> const & other) = delete;

  private:
    Transition(Guard_t guard, State<CORE> & next);
    ~Transition() noexcept;

    bool check(CORE * core) const;

    State<CORE> & next;
    Guard_t guard;
  };

}}

#include "ipp/Transition.ipp"

#endif
