/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_SERVICE_HPP
#define MAUVE_RUNTIME_SERVICE_HPP

#include <string>
#include "AbstractPort.hpp"

namespace mauve {
  namespace runtime {

  class AbstractInterface;

  /** A Service */
  class Service {
  public:
    Service() = delete;
    /** Constructor */
    Service(std::string const & name);
    /** Constructor */
    Service(const Service & other) = delete;
    virtual ~Service() noexcept;

    /** Service name */
    const std::string name;
    /** Service type name */
    std::string type_name() const;

    virtual connection_type get_type() const = 0;

    virtual std::string get_resource_name() const = 0;
  };

  }
} /* namespace mauve */

#endif
