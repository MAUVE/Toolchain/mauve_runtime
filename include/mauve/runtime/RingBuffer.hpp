/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_RING_BUFFER_HPP
#define MAUVE_RUNTIME_RING_BUFFER_HPP

#include "Shell.hpp"
#include "Core.hpp"
#include "Interface.hpp"
#include "Resource.hpp"

#include "ReadService.hpp"
#include "WriteService.hpp"

#include "DataStatus.hpp"
#include "RtMutex.hpp"

namespace mauve {
  namespace runtime {

  template <typename T>
  class ReadPort;

  template <typename T>
  class WritePort;


    // ========================= Shell =========================

      template <typename T>
      struct RingBufferShell: public Shell {
        Property<T> & default_value;   /// Default value used to fill buffer
        Property<size_t> & size;       /// Size of the ring buffer

        RingBufferShell(size_t size, T value);
        RingBufferShell() = delete;
        RingBufferShell(const RingBufferShell & other) = delete;
      };

      // ========================= Core =========================

      template <typename T>
      struct RingBufferCore: public Core<RingBufferShell<T>> {

        RingBufferCore();
        RingBufferCore(const RingBufferCore & other) = delete;

      public:
        bool configure_hook() override;
        void cleanup_hook() override;

        StatusValue<T> read();
        T read_value();
        DataStatus read_status();
        size_t read_length();
        void write(T value);
        void reset();

/*
        std::string to_string() const override {
          std::stringstream ss;
          ss << "value = " << value << " status = " << status;
          return ss.str();
        }
*/
      private:
        size_t size;
        T default_value;
        T * values;
        size_t begin;
        size_t end;
        DataStatus status;
        RtMutex mutex;
      };

    // ========================= Interface =========================

    template <typename T>
    struct RingBufferInterface: public Interface<RingBufferShell<T>, RingBufferCore<T>> {
      using C = RingBufferCore<T>;
      // ---------- Event ----------
      EventService & reset = this->mk_event_service("reset", &C::reset);
      // ---------- Reader ----------
      ReadService<StatusValue<T>> & read    = this->template mk_read_service<StatusValue<T>>("read", &C::read);
      ReadService<T> & read_value           = this->template mk_read_service<T>("read_value", &C::read_value);
      ReadService<DataStatus> & read_status = this->template mk_read_service<DataStatus>("read_status", &C::read_status);
      ReadService<size_t> & read_length = this->template mk_read_service<size_t>("read_length", &C::read_length);
      // ---------- Writer ----------
      WriteService<T> & write = this->template mk_write_service<T>("write", &C::write);
    };

    // ========================= Resource =========================

    template <typename T>
    using RingBuffer = Resource<RingBufferShell<T>, RingBufferCore<T>, RingBufferInterface<T>>;

  }

/*
  template <typename T>
  class RingBuffer final: public Resource {
  public:
    RingBuffer() = delete;
    RingBuffer(std::string const & name, size_t size, T default_value);
    RingBuffer(RingBuffer const & other) = delete;
    ~RingBuffer() noexcept;

    const size_t size;
    const T default_value;

    void clear() override;

    std::string type_name() const;
    std::string display()   const override;

    // ---------- Reader ----------

    ReadService<StatusValue<T>> & read     = mk_read_service<StatusValue<T>>("read", [this](){ return read_action(); });
    ReadService<T> & read_value           = mk_read_service<T>("read_value", [this](){ return read_value_action(); });
    ReadService<DataStatus> & read_status = mk_read_service<DataStatus>("read_status", [this](){ return read_status_action(); });

    // ---------- Writer ----------

    WriteService<T> & write_service = mk_write_service<T>("write_service", [this](T value){ write_action(value); });

    // ---------- Event ----------

    EventService & reset = mk_event_service("reset", [this](){ reset_action(); });

  private:
    T * values;
    size_t begin;
    size_t end;
    DataStatus status;

    StatusValue<T> read_action();
    T read_value_action();
    DataStatus read_status_action();
    void write_action(T value);
    void reset_action();
  };
*/

} /* namespace mauve */

#include "ipp/RingBuffer.ipp"

#endif
