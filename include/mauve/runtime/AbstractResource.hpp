/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_ABSTRACT_RESOURCE_HPP
#define MAUVE_RUNTIME_ABSTRACT_RESOURCE_HPP

#include "Configurable.hpp"
#include "WithName.hpp"
#include "WithAbstractShell.hpp"
#include "WithAbstractCore.hpp"
#include "WithAbstractInterface.hpp"

#include <string>

namespace mauve {
  namespace runtime {

    class Service;

    /**
    * Abstract Resource class
    */
    class AbstractResource
    : virtual public Configurable
    , virtual public WithName
    , virtual public WithAbstractShell
    , virtual public WithAbstractCore
    , virtual public WithAbstractInterface
    {
    public:
      AbstractResource() = delete;
      /** Constructor
       * \param name resource name
       */
      AbstractResource(std::string const & name);
      /** Constructor
       * \param other a Resource
       */
      AbstractResource(AbstractResource const & other) = delete;
      virtual ~AbstractResource() noexcept = 0;

      /** Get resource name */
      std::string name() const override final { return _name; };
      /** Get resource type name */
      std::string type_name() const;

      /** Check if the resource is empty */
      virtual bool is_empty     () const = 0;

      /** Clear the resource */
      virtual bool clear() = 0;

      virtual int get_service_index(const Service* service) const = 0;

    private:
      std::string _name;
    };
  }
}

#endif
