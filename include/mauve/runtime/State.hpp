/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_STATE_HPP
#define MAUVE_RUNTIME_STATE_HPP

#include "common.hpp"
#include "AbstractState.hpp"

#include <string>
#include <vector>
#include <functional>

namespace mauve {
  namespace runtime {

  template <typename CORE>
  class State : public AbstractState {

  public:
    template <typename S, typename C>
    friend class FiniteStateMachine;

    template <typename S, typename C, typename F>
    friend class Component;

    State() = delete;
    State(State<CORE> const & other) = delete;
    State<CORE> & operator=(State<CORE> const & other) = delete;

    void set_next(State<CORE> & next);

  protected:
    State(AbstractFiniteStateMachine* container, std::string const & name);
    virtual ~State() noexcept;

    State<CORE> * next;

  private:
    virtual State<CORE> * run(CORE * core) = 0;
    virtual std::vector<State<CORE>*> next_states() const = 0;
  };

}}

#include "ipp/State.ipp"

#endif
