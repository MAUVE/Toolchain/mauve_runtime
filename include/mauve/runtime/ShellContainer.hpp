/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_SHELL_CONTAINER_HPP
#define MAUVE_RUNTIME_SHELL_CONTAINER_HPP

#include "WithAbstractShell.hpp"
#include "WithLogger.hpp"
#include "WithName.hpp"

namespace mauve {
  namespace runtime {

    struct ShellContainer
    : virtual public WithLogger
    , virtual public WithAbstractShell
    , virtual public WithName
    {};
  }
}

#endif
