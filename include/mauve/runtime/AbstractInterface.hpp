/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_ABSTRACT_INTERFACE_HPP
#define MAUVE_RUNTIME_ABSTRACT_INTERFACE_HPP

#include "Configurable.hpp"
#include "HasProperty.hpp"
#include <string>

namespace mauve {
  namespace runtime {

    class AbstractResource;

    class Service;

    /**
    * Abstract Interface class
    */
    class AbstractInterface
    : virtual Configurable
    , public HasProperty
    {
    public:
      /** Get interface type name */
      std::string type_name() const;

      /** Get the set of services owned by the interface */
      virtual const std::vector<Service*> get_services() const = 0;

      virtual std::size_t get_services_size() const = 0;

      /** Get a service by name */
      virtual Service* get_service(std::string const & name) const = 0;

      virtual Service* get_service(int index) const = 0;

      virtual int get_service_index(const Service* service) const = 0;
    };
  }
}

#endif
