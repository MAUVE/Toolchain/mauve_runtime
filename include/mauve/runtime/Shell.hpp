/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_SHELL_HPP
#define MAUVE_RUNTIME_SHELL_HPP

#include "HasProperty.hpp"
#include "HasPort.hpp"
#include "AbstractPort.hpp"
#include "ShellContainer.hpp"
#include "WithLogger.hpp"
#include "Configurable.hpp"
#include "WithHook.hpp"
#include "WithName.hpp"

#include <exception>
#include <string>
#include <vector>
#include <type_traits>

namespace mauve {
  namespace runtime {

    class AbstractLogger;

    // ========================= Shell =========================

    /**
    * A Shell is the interface of a component.
    *
    * A Shell declares elements that are visible at the architecture level
    * in order to have the components interact one with the other.
    */
    class Shell
    : virtual public HasProperty
    , virtual public HasPort
    , virtual public WithLogger
    , virtual public Configurable
    , virtual public WithHook
    , virtual public WithName
    {

    public:

      template <typename S, typename C, typename F>
      friend class Component;
      template <typename S, typename C, typename I>
      friend class Resource;

      /**
      * Copy constructor.
      *
      * \remark deleted
      */
      Shell(const Shell& shell) = delete;

      /**
      * Get the Shell type name.
      * \return the Shell type name
      */
      std::string type_name() const;

      inline AbstractLogger& logger() const override { return _container->logger(); }

      inline bool is_configured() const override final { return _configured; }

      inline bool configure() override final { return _container->configure_shell(); }
      inline void cleanup()   override final { _container->cleanup_shell(); }

      /**
      * Print the Shell model.
      * \param out the output stream in which the print is done
      */
      void print_model(std::ostream& out) const;

      /**
       * Get Component name
       */
      virtual std::string name() const override final {
        return this->_container->name(); };

    protected:
      /** Default constructor. */
      Shell();
      /** Default destructor. */
      virtual ~Shell() noexcept;

    private:
      ShellContainer* _container;
      bool _configured;

      bool _configure();
      void _cleanup();
    };

    /**
    * Stream a shell to an output stream.
    * \param out the output stream
    * \param shell the shell to be streamed
    * \return \a out
    */
    std::ostream& operator<<(std::ostream& out, Shell const & shell);

  }
}

#endif // SHELL_HPP
