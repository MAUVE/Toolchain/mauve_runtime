/*
* Copyright 2017 ONERA
*
* This file is part of the MAUVE Runtime project.
*
* MAUVE Runtime is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License version 3 as
* published by the Free Software Foundation.
*
* MAUVE Runtime is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
*/
#ifndef MAUVE_RUNTIME_WRITE_SERVICE_HPP
#define MAUVE_RUNTIME_WRITE_SERVICE_HPP

#include "Service.hpp"
#include <functional>

namespace mauve {
  namespace runtime {

    template <typename C>
    struct ServiceContainer;

    template <typename T>
    class WriteService : public Service {
    public:
      WriteService(std::string const & name): Service{name} {}
      virtual ~WriteService() noexcept {}

      WriteService() = delete;
      WriteService(const WriteService<T> & other) = delete;

      connection_type get_type() const override final { return WRITE; }

      virtual void write(T value) const = 0;
      inline void operator()(T value) const { write(value); }
    };

    template <typename CORE, typename T>
    class WriteServiceImpl final : public WriteService<T> {
    public:
      using action_t = std::function<void(CORE *, T)>;

      WriteServiceImpl(ServiceContainer<CORE> * container, std::string const & name, action_t action);
      ~WriteServiceImpl() noexcept;

      WriteServiceImpl() = delete;
      WriteServiceImpl(const WriteServiceImpl<CORE, T> & other) = delete;

      std::string get_resource_name() const override { return container->name(); }

      void write(T value) const override;

    private:
      ServiceContainer<CORE> * container;
      action_t action;
    };

  }
} /* namespace mauve */

#include "ipp/WriteService.ipp"

#endif
