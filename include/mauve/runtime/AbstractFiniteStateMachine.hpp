/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_ABSTRACT_FINITE_STATE_MACHINE_HPP
#define MAUVE_RUNTIME_ABSTRACT_FINITE_STATE_MACHINE_HPP

#include "common.hpp"
#include "Configurable.hpp"
#include "HasProperty.hpp"
#include <string>

namespace mauve {
  namespace runtime {

    class AbstractState;

    /**
    * A Finite State Machine describes the component behavior.
    */
    class AbstractFiniteStateMachine
    : virtual public Configurable
    , public HasProperty
    {
    public:
      template <typename S, typename C, typename F>
      friend class Component;

      /** Default constructor. */
      AbstractFiniteStateMachine();
      /** Default destructor. */
      virtual ~AbstractFiniteStateMachine();

      /**
      * Get the type of this FSM
      * \return the type name
      */
      std::string type_name() const;

      /**
      * Get the type of the FSM shell.
      * \return the type name of the shell
      */
      virtual std::string shell_type_name() const = 0;

      /**
      * Get the type of the FSM core.
      * \return the type name of the core
      */
      virtual std::string core_type_name () const = 0;

      /**
      * \return Get the states number
      */
      virtual std::size_t get_states_size() const = 0;

      /**
      * Get a state of the FSM by name
      * \return the AbstractState named "name" if exists (nullptr if not)
      */
      virtual AbstractState* get_state(std::string const & name) const = 0;

      /**
      * Get a state of the FSM by state index
      * \return the AbstractState if exists (nullptr if not)
      */
      virtual AbstractState* get_state(int index) const = 0;

      /**
      * Get a index of state in the FSM
      * \return index if exists (-1 if not)
      */
      virtual int get_state_index(const AbstractState* state) const = 0;
    };

  }
} /* namespace mauve */

#endif
