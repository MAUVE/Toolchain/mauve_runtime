/*
 * Copyright 2018 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_PROPERTY_TREE_HPP
#define MAUVE_RUNTIME_PROPERTY_TREE_HPP

#include <yaml-cpp/yaml.h>

#include "AbstractDeployer.hpp"
#include "WithLogger.hpp"

namespace mauve {
  namespace runtime {

  class PropertyTree {
  public:
    static void add(std::istream& is);
    static void add(const std::string& s);
    static void add(const YAML::Node& n);

    static void clear();

    static const YAML::Node& get();

    template <typename T>
    static bool configure(T* element,
      const YAML::Node& properties = PropertyTree::get(),
      WithLogger* l = AbstractDeployer::instance());

  private:
    static YAML::Node property_tree_;
    static const YAML::Node & cnode(const YAML::Node &n);
    static YAML::Node merge(const YAML::Node& a, const YAML::Node&b);
  };

}} //ns
#endif
