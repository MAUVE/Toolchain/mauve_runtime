/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_SHARED_DATA_HPP
#define MAUVE_RUNTIME_SHARED_DATA_HPP

#include "Shell.hpp"
#include "Core.hpp"
#include "Interface.hpp"
#include "Resource.hpp"

#include "DataStatus.hpp"
#include "ReadService.hpp"
#include "WriteService.hpp"

#include "DataStatus.hpp"
#include "RtMutex.hpp"

#include <tuple>
#include <sstream>

namespace mauve {
  namespace runtime {

    template <typename T>
    class ReadPort;

    template <typename T>
    class WritePort;

    // ========================= Shell =========================

    template <typename T>
    struct SharedDataShell: public Shell {
      Property<T> & default_value;

      SharedDataShell(T value);
      SharedDataShell() = delete;
      SharedDataShell(const SharedDataShell & other) = delete;
    };

    // ========================= Core =========================

    template <typename T>
    struct SharedDataCore: public Core<SharedDataShell<T>> {

      SharedDataCore();
      SharedDataCore(const SharedDataCore & other) = delete;

    public:
      bool configure_hook() override;

      StatusValue<T> read();
      T read_value();
      DataStatus read_status();
      void write(T value);
      void reset();

      std::string to_string() const override { 
        std::stringstream ss;
        ss << "value = " << value << " status = " << status; 
        return ss.str();
      }

    private:
      T value;
      DataStatus status;
      RtMutex mutex;
    };

    // ========================= Interface =========================

    template <typename T>
    struct SharedDataInterface: public Interface<SharedDataShell<T>, SharedDataCore<T>> {
      using C = SharedDataCore<T>;
      // ---------- Event ----------
      EventService & reset = this->mk_event_service("reset", &C::reset);
      // ---------- Reader ----------
      ReadService<StatusValue<T>> & read    = this->template mk_read_service<StatusValue<T>>("read", &C::read);
      ReadService<T> & read_value           = this->template mk_read_service<T>("read_value", &C::read_value);
      ReadService<DataStatus> & read_status = this->template mk_read_service<DataStatus>("read_status", &C::read_status);
      // ---------- Writer ----------
      WriteService<T> & write = this->template mk_write_service<T>("write", &C::write);
    };

    // ========================= Resource =========================

    template <typename T>
    using SharedData = Resource<SharedDataShell<T>, SharedDataCore<T>, SharedDataInterface<T>>;

  }
} /* namespace mauve */

#include "ipp/SharedData.ipp"

#endif
