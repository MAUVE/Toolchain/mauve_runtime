/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_ABSTRACT_CORE_HPP
#define MAUVE_RUNTIME_ABSTRACT_CORE_HPP

#include "Configurable.hpp"
#include "mauve/runtime/HasProperty.hpp"
#include <string>

namespace mauve {
  namespace runtime {

    class ComponentLogger;
    template <typename CORE>
    class ExecState;
    template <typename CORE>
    class SynchroState;

    /**
    * The Core defines the methods of the component.
    */
    class AbstractCore
    : virtual public Configurable
    , public HasProperty
    {
    public:
      template <typename S, typename C, typename F>
      friend class Component;

      /** Default constructor. */
      AbstractCore();
      /** Default descructor. */
      virtual ~AbstractCore() noexcept;

      /**
      * Get type name.
      * \return the type name of this core
      */
      std::string type_name() const;
      /**
      * Get the shell type name.
      * \return the type name of this instantiated shell
      */
      virtual std::string shell_type_name() const = 0;

      /** Get the core container name */
      virtual std::string container_name() const = 0;

      virtual std::string to_string() const { return "?"; }
    };

  }
} /* namespace mauve */

#endif
