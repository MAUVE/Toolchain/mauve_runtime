/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_RUNTIME_HPP
#define MAUVE_RUNTIME_HPP

#include "runtime/AbstractComponent.hpp"
#include "runtime/AbstractCore.hpp"
#include "runtime/AbstractDeployer.hpp"
#include "runtime/AbstractFiniteStateMachine.hpp"
#include "runtime/AbstractPort.hpp"
#include "runtime/AbstractState.hpp"
#include "runtime/Architecture.hpp"
#include "runtime/CallPort.hpp"
#include "runtime/CallService.hpp"
#include "runtime/common.hpp"
#include "runtime/Component.hpp"
#include "runtime/Configurable.hpp"
#include "runtime/Core.hpp"
#include "runtime/DataStatus.hpp"
#include "runtime/Deployer.hpp"
#include "runtime/EventPort.hpp"
#include "runtime/EventService.hpp"
#include "runtime/ExecState.hpp"
#include "runtime/FiniteStateMachine.hpp"
#include "runtime/HasProperty.hpp"
#include "runtime/Manager.hpp"
#include "runtime/PeriodicStateMachine.hpp"
#include "runtime/Port.hpp"
#include "runtime/Property.hpp"
#include "runtime/ReadPort.hpp"
#include "runtime/ReadService.hpp"
#include "runtime/Resource.hpp"
#include "runtime/RingBuffer.hpp"
#include "runtime/RtMutex.hpp"
#include "runtime/Service.hpp"
#include "runtime/SharedData.hpp"
#include "runtime/Shell.hpp"
#include "runtime/State.hpp"
#include "runtime/SynchroState.hpp"
#include "runtime/Task.hpp"
#include "runtime/Transition.hpp"
#include "runtime/WritePort.hpp"
#include "runtime/WriteService.hpp"

#include "runtime/logging/logger.hpp"
#include "runtime/main.hpp"
#include "runtime/PropertyTree.hpp"

/**
 * The MAUVE namespace.
 */
namespace mauve {
  /**
   * The \a runtime namespace contains the core elements of the MAUVE middleware for designing and deploying real-time architectures
   */
  namespace runtime {

  }
}

#endif
