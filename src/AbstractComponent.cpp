/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */

#include "mauve/runtime/Component.hpp"
#include "mauve/runtime/logging/logger.hpp"

#include <typeinfo>
#include <cxxabi.h>

namespace mauve {
  namespace runtime {

    // ------------------------- Constructor -------------------------

    AbstractComponent::AbstractComponent(std::string const & name)
    : logger { new ComponentLogger(this) }
    , _name  { name }
    {
    }

    // ------------------------- Constructor -------------------------

    AbstractComponent::~AbstractComponent() {}

    // ------------------------- Method -------------------------

    std::string AbstractComponent::type_name() const {
      int status = -4; // some arbitrary value to eliminate the compiler warning
      std::string name = abi::__cxa_demangle(typeid(*this).name(), nullptr, nullptr, &status);
      return name;
    }

    // ------------------------- Operator -------------------------

  }
}
