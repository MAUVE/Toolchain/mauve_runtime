/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime/Architecture.hpp"
#include "mauve/runtime/Component.hpp"
#include "mauve/runtime/Resource.hpp"
#include <typeinfo>
#include <cxxabi.h>

namespace mauve {
  namespace runtime {

    // ------------------------- Constructor -------------------------

    Architecture::Architecture()
    : _configured { false }
    , logger_ { new DeployerLogger() }
    {
    }

    // ------------------------- Destructor -------------------------

    Architecture::~Architecture() {
      cleanup();
      for (AbstractResource * res: resources) {
        delete res;
      }
      for (AbstractComponent * cpt: components) {
        delete cpt;
      }
    }

    // ------------------------- Method -------------------------

    std::string Architecture::type_name() const {
      int status = -4; // some arbitrary value to eliminate the compiler warning
      return abi::__cxa_demangle(typeid(*this).name(), nullptr, nullptr, &status);
    }

    bool Architecture::configure() {
      if (!is_configured()) {
        logger().info("Configuring architecture", _configured);
        _configured = configure_hook();
      }
      return _configured;
    }

    void Architecture::cleanup() {
      if (is_configured()) {
        logger().info("Cleaning architecture");
        cleanup_hook();
        _configured = false;
      }
    }

    bool Architecture::configure_hook() {
      bool ok = true;
      for (auto& res: resources) {
        if (!res->configure()) ok = false;
      }
      for (auto& cpt: components) {
        if (!cpt->configure()) ok = false;
      }
      return ok;
    }

    void Architecture::cleanup_hook() {
      for (auto& res: resources) {
        res->cleanup();
      }
      for (auto& cpt: components) {
        cpt->cleanup();
        cpt->disconnect();
      }
    }

    // ---------- Component ----------

    const std::vector<AbstractComponent*> Architecture::get_components() {
      return components;
    }

    AbstractComponent* Architecture::get_component(int id) const {
      if (id < 0 || (unsigned)id >= components.size()) return nullptr;
      return components[(unsigned)id];
    }

    AbstractComponent* Architecture::get_component(const std::string& name) const {
      for (std::size_t index = 0; index < components.size(); index++) {
        AbstractComponent* comp = components[index];
        if (comp->name() == name) return comp;
      }
      return nullptr;
    }

    // ---------- Resource ----------

    const std::vector<AbstractResource*> Architecture::get_resources() {
      return resources;
    }

    AbstractResource* Architecture::get_resource(int id) const {
      if (id < 0 || (unsigned)id >= resources.size()) return nullptr;
      return resources[(unsigned)id];
    }

    AbstractResource* Architecture::get_resource(const std::string & name) const {
      for (std::size_t index = 0; index < resources.size(); index++) {
        AbstractResource* resource = resources[index];
        if (resource->name() == name) return resource;
      }
      return nullptr;
    }

    int Architecture::get_resource_index(const std::string & name) const {
      for (std::size_t index = 0; index < resources.size(); index++) {
        if (resources[index]->name() == name) return (signed)index;
      }
      return -1;
    }

    int Architecture::get_resource_index(const AbstractResource* resource) const {
      for (std::size_t index = 0; index < resources.size(); index++) {
        if (resources[index] == resource) return (signed)index;
      }
      return -1;
    }

  }
} /* namespace mauve */
