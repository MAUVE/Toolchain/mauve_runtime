/*
 * Copyright 2018 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */

#include <iostream>
#include <fstream>

#include <yaml-cpp/exceptions.h>

#include "args/args.hxx"
#include "mauve/runtime/main.hpp"
#include "mauve/runtime/logging/logger.hpp"
#include "mauve/runtime/PropertyTree.hpp"

namespace mauve {
namespace runtime {

int main(int argc, char** argv) {
  args::ArgumentParser parser("MAUVE Runtime Deployment", "Copyright ONERA 2017-2018; Visit https://mauve.gitlab.io");
  args::HelpFlag help(parser, "help", "Display the help menu", {'h', "help"});
  args::ValueFlagList<std::string> config(parser, "FILE", "Property configuration files", {'p', "properties"});
  args::Group log_group(parser, "Logger options (exclusive):", args::Group::Validators::Xor);
  args::ValueFlag<std::string> logger(log_group, "FILE", "Logger configuration file", {'l', "logger"});
  args::ValueFlag<std::string> loglevel(log_group, "LEVEL", "Configures the default logger to stdout with the given level", {"level"});

  try {
      parser.ParseCLI(argc, argv);
  } catch (args::Help) {
      std::cout << parser;
      return 1;
  } catch (args::ParseError e) {
      std::cerr << e.what() << std::endl;
      std::cerr << parser;
      return 1;
  } catch (args::ValidationError e) {
      std::cerr << e.what() << std::endl;
      std::cerr << parser;
      return 1;
  }

  if (logger) {
    std::ifstream f;
    f.open(args::get(logger));
    if (f.is_open()) {
      try { AbstractLogger::initialize(f); }
      catch (YAML::ParserException e) {
        std::cerr << "Error when loading logger config file " << args::get(logger) << std::endl;
        std::cerr << e.what();
        return 1;
      }
    }
    else {
      std::cerr << "Error opening file " << args::get(logger) << std::endl;
      std::cerr << parser;
      return 1;
    }
  }

  if (loglevel) {
    YAML::Node cfg;
    cfg["default"];
    cfg["default"]["level"] = args::get(loglevel);
    cfg["default"]["type"] = "stdout";
    try { AbstractLogger::initialize(cfg); }
    catch (YAML::ParserException e) {
      std::cerr << "Error when loading configuration " << cfg << std::endl;
      std::cerr << e.what();
      return 1;
    }
  }

  if (config) {
    std::ifstream f;
    for (const auto file: args::get(config)) {
      f.open(file);
      if (f.is_open()) {
        try { PropertyTree::add(f); }
        catch (YAML::ParserException e) {
          std::cerr << "Error when loading properties config file " << file << std::endl;
          std::cerr << e.what();
        }
      }
      else {
        std::cerr << "Error opening file " << file << std::endl;
        std::cerr << parser;
      }
      f.close();
    }
  }

  return 0;
}

}}
