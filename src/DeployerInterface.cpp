/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime/DeployerInterface.hpp"
#include "mauve/runtime/Architecture.hpp"

using namespace mauve::runtime;

void free_string_cache() {
  if (string_cache != nullptr) {
    free(string_cache);
    string_cache = nullptr;
  }
}

char* add_string_cache(std::string str) {
  free_string_cache();
  string_cache = strndup(str.c_str(), str.size());
  return string_cache;
}

// Deployer
//--------------------------------------------------

long deployer_now() {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return 0;
  return (long) deployer->now();
}

long deployer_get_time() {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return 0;
  return (long) deployer->get_time();
}

int deployer_create_tasks() {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return 0;
  return deployer->create_tasks();
}

int deployer_create_task(int cpt_id) {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return 0;
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return 0;
  if (deployer->create_task(cpt)) return 1;
  else return 0;
}

void deployer_clear_tasks() {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return;
  deployer->clear_tasks();
}

int deployer_clear_task(int cpt_id) {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return 0;
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return 0;
  if (deployer->clear_task(cpt)) return 1;
  else return 0;
}

int deployer_activate() {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return 0;
  return deployer->activate();
}

int deployer_activate_component(int cpt_id) {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return 0;
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return 0;
  return deployer->activate(cpt);
}

int deployer_start() {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return 0;
  return deployer->start();
}

int deployer_start_component(int cpt_id, long start_time) {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return 0;
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return 0;
  return deployer->start(cpt, (unsigned)start_time);
}

void deployer_stop() {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return;
  return deployer->stop();
}

int deployer_stop_component(int cpt_id) {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return 0;
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return 0;
  if (deployer->stop(cpt)) return 1;
  else return 0;
}

// Logger
//--------------------------------------------------

void logger_initialize(const char* config) {
  std::string str = config;
  AbstractLogger::initialize(str);
}

// Architecture
//--------------------------------------------------

const char* architecture_type_name() {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return nullptr;
  Architecture* architecture = deployer->get_architecture();
  if (architecture == nullptr) return nullptr;
  return add_string_cache(architecture->type_name());
}


int architecture_is_configured() {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return 0;
  Architecture* architecture = deployer->get_architecture();
  if (architecture == nullptr) return 0;
  if (architecture->is_configured()) return 1;
  else return 0;
}

int architecture_configure() {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return 0;
  Architecture* architecture = deployer->get_architecture();
  if (architecture == nullptr) return 0;
  if (architecture->configure()) return 1;
  else return 0;
}

void architecture_cleanup() {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return;
  Architecture* architecture = deployer->get_architecture();
  if (architecture == nullptr) return;
  architecture->cleanup();
}

int architecture_components_number() {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return 0;
  Architecture* architecture = deployer->get_architecture();
  if (architecture == nullptr) return 0;
  return (int)architecture->get_components_size();
}

int architecture_resources_number() {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return 0;
  Architecture* architecture = deployer->get_architecture();
  if (architecture == nullptr) return 0;
  return (int)architecture->get_resources_size();
}

// Component
//--------------------------------------------------

AbstractComponent* architecture_component(int cpt_id) {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return nullptr;
  Architecture* architecture = deployer->get_architecture();
  if (architecture == nullptr) return nullptr;
  return architecture->get_component(cpt_id);
}

const char* component_name(int cpt_id) {
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return nullptr;
  return add_string_cache(cpt->name());
}

const char* component_type_name(int cpt_id) {
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return nullptr;
  return add_string_cache(cpt->type_name());
}

int component_is_configured(int cpt_id) {
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return 0;
  if (cpt->is_configured()) return 1;
  else return 0;
}

int component_configure(int cpt_id) {
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return 0;
  if (cpt->configure()) return 1;
  else return 0;
}

void component_cleanup(int cpt_id) {
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return;
  cpt->cleanup();
}

int component_is_empty(int cpt_id) {
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return 0;
  if (cpt->is_empty()) return 1;
  else return 0;
}

int component_is_created(int cpt_id) {
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return 0;
  if (cpt->is_created()) return 1;
  else return 0;
}

int component_is_activated(int cpt_id) {
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return 0;
  if (cpt->is_activated()) return 1;
  else return 0;
}

int component_is_running(int cpt_id) {
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return 0;
  if (cpt->is_running()) return 1;
  else return 0;
}

int component_priority(int cpt_id) {
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return -1;
  return cpt->get_priority();
}

int component_set_priority(int cpt_id, int priority) {
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return 0;
  if (cpt->set_priority(priority)) return 1;
  else return 0;
}

int component_cpu(int cpt_id) {
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return -1;
  return cpt->get_cpu();
}

int component_set_cpu(int cpt_id, int cpu) {
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return 0;
  if (cpt->set_cpu(cpu)) return 1;
  else return 0;
}

long component_clock(int cpt_id) {
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return -1;
  return (long) cpt->get_time();
}

// Component Shell
//--------------------------------------------------

Shell* component_shell(int cpt_id) {
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return nullptr;
  return cpt->get_shell();
}

const char* component_shell_type_name(int cpt_id) {
  Shell* shell = component_shell(cpt_id);
  if (shell == nullptr) return nullptr;
  return add_string_cache(shell->type_name());
}

int component_shell_is_configured(int cpt_id) {
  Shell* shell = component_shell(cpt_id);
  if (shell == nullptr) return false;
  return shell->is_configured();
}

int component_shell_configure(int cpt_id) {
  Shell* shell = component_shell(cpt_id);
  if (shell == nullptr) return false;
  return shell->configure();
}

void component_shell_cleanup(int cpt_id) {
  Shell* shell = component_shell(cpt_id);
  if (shell == nullptr) return;
  shell->configure();
}

// Component Shell Property
//--------------------------------------------------

AbstractProperty* component_shell_property(int cpt_id, int prop_id) {
  Shell* shell = component_shell(cpt_id);
  if (shell == nullptr) return nullptr;
  return shell->get_property(prop_id);
}

int component_shell_properties_number(int cpt_id) {
  Shell* shell = component_shell(cpt_id);
  if (shell == nullptr) return 0;
  return (int)shell->get_properties_size();
}

const char* component_shell_property_name(int cpt_id, int prop_id) {
  AbstractProperty* property = component_shell_property(cpt_id, prop_id);
  if (property == nullptr) return nullptr;
  return add_string_cache(property->name);
}

const char* component_shell_property_type_name(int cpt_id, int prop_id) {
  AbstractProperty* property = component_shell_property(cpt_id, prop_id);
  if (property == nullptr) return nullptr;
  return add_string_cache(property->type_name());
}

int component_shell_property_type(int cpt_id, int prop_id) {
  AbstractProperty* property = component_shell_property(cpt_id, prop_id);
  if (property == nullptr) return -1;
  switch (property->get_type()) {
    case INTEGRAL: return 0;
    case FLOATING_POINT: return 1;
    case STRING: return 2;
    case OTHER: return 3;
    default: return -1;
  }
}

long component_shell_property_get_integral_value(int cpt_id, int prop_id) {
  AbstractProperty* property = component_shell_property(cpt_id, prop_id);
  if (property == nullptr) return 0L;
  if (property->get_type() != INTEGRAL) return 0L;
  return ((AbstractIntegralProperty*)property)->get();
}

int component_shell_property_set_integral_value(int cpt_id, int prop_id, long value) {
  AbstractProperty* property = component_shell_property(cpt_id, prop_id);
  if (property == nullptr) return 0;
  if (property->get_type() != INTEGRAL) return 0;
  return ((AbstractIntegralProperty*)property)->set(value);
}

double component_shell_property_get_floating_value(int cpt_id, int prop_id) {
  AbstractProperty* property = component_shell_property(cpt_id, prop_id);
  if (property == nullptr) return 0.0;
  if (property->get_type() != FLOATING_POINT) return 0.0;
  return ((AbstractFloatingPointProperty*)property)->get();
}

int component_shell_property_set_floating_value(int cpt_id, int prop_id, double value) {
  AbstractProperty* property = component_shell_property(cpt_id, prop_id);
  if (property == nullptr) return 0;
  if (property->get_type() != FLOATING_POINT) return 0;
  return ((AbstractFloatingPointProperty*)property)->set(value);
}

const char* component_shell_property_get_string_value(int cpt_id, int prop_id) {
  AbstractProperty* property = component_shell_property(cpt_id, prop_id);
  if (property == nullptr) return nullptr;
  if (property->get_type() != STRING) return nullptr;
  return add_string_cache(((StringProperty*)property)->get_value());
}

int component_shell_property_set_string_value(int cpt_id, int prop_id, const char* value) {
  AbstractProperty* property = component_shell_property(cpt_id, prop_id);
  if (property == nullptr) return 0;
  if (property->get_type() != STRING) return 0;
  return ((StringProperty*)property)->set_value(value);
}

// Component Shell Port
//--------------------------------------------------

AbstractPort* component_shell_port(int cpt_id, int port_id) {
  Shell* shell = component_shell(cpt_id);
  if (shell == nullptr) return nullptr;
  return shell->get_port(port_id);
}

int component_shell_ports_number(int cpt_id) {
  Shell* shell = component_shell(cpt_id);
  if (shell == nullptr) return 0;
  return (int)shell->get_ports_size();
}

const char* component_shell_port_name(int cpt_id, int port_id) {
  AbstractPort* port = component_shell_port(cpt_id, port_id);
  if (port == nullptr) return nullptr;
  return add_string_cache(port->name);
}

const char* component_shell_port_type_name(int cpt_id, int port_id) {
  AbstractPort* port = component_shell_port(cpt_id, port_id);
  if (port == nullptr) return nullptr;
  return add_string_cache(port->type_name());
}

int component_shell_port_type(int cpt_id, int port_id) {
  AbstractPort* port = component_shell_port(cpt_id, port_id);
  if (port == nullptr) return -1;
  switch (port->get_type()) {
    case EVENT: return 0;
    case READ: return 1;
    case WRITE: return 2;
    case CALL: return 3;
    default: return -1;
  }
}

int component_shell_port_is_connected(int cpt_id, int port_id) {
  AbstractPort* port = component_shell_port(cpt_id, port_id);
  if (port == nullptr) return 0;
  return port->is_connected();
}

int component_shell_port_connections_number(int cpt_id, int port_id) {
  AbstractPort* port = component_shell_port(cpt_id, port_id);
  if (port == nullptr) return 0;
  return (int) port->connections_size();
}

int component_shell_port_connection_resource(int cpt_id, int port_id, int serv_id) {
  AbstractPort* port = component_shell_port(cpt_id, port_id);
  if (port == nullptr) return -1;
  Service* service = port->get_connection(serv_id);
  if (service == nullptr) return -2;
  auto res_name = service->get_resource_name();
  AbstractDeployer* deployer = AbstractDeployer::instance();
  Architecture* architecture = deployer->get_architecture();
  return architecture->get_resource_index(res_name);
}

int component_shell_port_connection_service(int cpt_id, int port_id, int serv_id) {
  AbstractPort* port = component_shell_port(cpt_id, port_id);
  if (port == nullptr) return -1;
  Service* service = port->get_connection(serv_id);
  if (service == nullptr) return -2;
  auto res_name = service->get_resource_name();
  AbstractDeployer* deployer = AbstractDeployer::instance();
  Architecture* architecture = deployer->get_architecture();
  AbstractResource* resource = architecture->get_resource(res_name);
  return resource->get_service_index(service);
}

int component_shell_port_connect(int cpt_id, int port_id, int res_id, int serv_id) {
  AbstractPort* port = component_shell_port(cpt_id, port_id);
  if (port == nullptr) return 0;
  Service* service = resource_interface_service(res_id, serv_id);
  if (service == nullptr) return 0;
  return port->connect_service(service);
}

int component_shell_port_disconnect(int cpt_id, int port_id) {
  AbstractPort* port = component_shell_port(cpt_id, port_id);
  if (port == nullptr) return 0;
  return port->disconnect();
}

// Component Core
//--------------------------------------------------

AbstractCore* component_core(int cpt_id) {
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return nullptr;
  return cpt->get_core();
}

const char* component_core_type_name(int cpt_id) {
  AbstractCore* core = component_core(cpt_id);
  if (core == nullptr) return nullptr;
  return add_string_cache(core->type_name());
}

int component_core_is_configured(int cpt_id) {
  AbstractCore* core = component_core(cpt_id);
  if (core == nullptr) return 0;
  if (core->is_configured()) return 1;
  else return 0;
}

int component_core_configure(int cpt_id) {
  AbstractCore* core = component_core(cpt_id);
  if (core == nullptr) return 0;
  if (core->configure()) return 1;
  else return 0;
}

void component_core_cleanup(int cpt_id) {
  AbstractCore* core = component_core(cpt_id);
  if (core == nullptr) return;
  core->cleanup();
}

// Component Core Property
//--------------------------------------------------

AbstractProperty* component_core_property(int cpt_id, int prop_id) {
  AbstractCore* core = component_core(cpt_id);
  if (core == nullptr) return nullptr;
  return core->get_property(prop_id);
}

int component_core_properties_number(int cpt_id) {
  AbstractCore* core = component_core(cpt_id);
  if (core == nullptr) return 0;
  return (int)core->get_properties_size();
}

const char* component_core_property_name(int cpt_id, int prop_id) {
  AbstractProperty* property = component_core_property(cpt_id, prop_id);
  if (property == nullptr) return nullptr;
  return add_string_cache(property->name);
}

const char* component_core_property_type_name(int cpt_id, int prop_id) {
  AbstractProperty* property = component_core_property(cpt_id, prop_id);
  if (property == nullptr) return nullptr;
  return add_string_cache(property->type_name());
}

int component_core_property_type(int cpt_id, int prop_id) {
  AbstractProperty* property = component_core_property(cpt_id, prop_id);
  if (property == nullptr) return -1;
  switch (property->get_type()) {
    case INTEGRAL: return 0;
    case FLOATING_POINT: return 1;
    case STRING: return 2;
    case OTHER: return 3;
    default: return -1;
  }
}

long component_core_property_get_integral_value(int cpt_id, int prop_id) {
  AbstractProperty* property = component_core_property(cpt_id, prop_id);
  if (property == nullptr) return 0L;
  if (property->get_type() != INTEGRAL) return 0L;
  return ((AbstractIntegralProperty*)property)->get();
}

int component_core_property_set_integral_value(int cpt_id, int prop_id, long value) {
  AbstractProperty* property = component_core_property(cpt_id, prop_id);
  if (property == nullptr) return 0;
  if (property->get_type() != INTEGRAL) return 0;
  return ((AbstractIntegralProperty*)property)->set(value);
}

double component_core_property_get_floating_value(int cpt_id, int prop_id) {
  AbstractProperty* property = component_core_property(cpt_id, prop_id);
  if (property == nullptr) return 0.0;
  if (property->get_type() != FLOATING_POINT) return 0.0;
  return ((AbstractFloatingPointProperty*)property)->get();
}

int component_core_property_set_floating_value(int cpt_id, int prop_id, double value) {
  AbstractProperty* property = component_core_property(cpt_id, prop_id);
  if (property == nullptr) return 0;
  if (property->get_type() != FLOATING_POINT) return 0;
  return ((AbstractFloatingPointProperty*)property)->set(value);
}

const char* component_core_property_get_string_value(int cpt_id, int prop_id) {
  AbstractProperty* property = component_core_property(cpt_id, prop_id);
  if (property == nullptr) return nullptr;
  if (property->get_type() != STRING) return nullptr;
  return add_string_cache(((StringProperty*)property)->get_value());
}

int component_core_property_set_string_value(int cpt_id, int prop_id, const char* value) {
  AbstractProperty* property = component_core_property(cpt_id, prop_id);
  if (property == nullptr) return 0;
  if (property->get_type() != STRING) return 0;
  return ((StringProperty*)property)->set_value(value);
}

// Component FSM
//--------------------------------------------------

AbstractFiniteStateMachine* component_fsm(int cpt_id) {
  AbstractComponent* cpt = architecture_component(cpt_id);
  if (cpt == nullptr) return nullptr;
  return cpt->get_fsm();
}

const char* component_fsm_type_name(int cpt_id) {
  AbstractFiniteStateMachine* fsm = component_fsm(cpt_id);
  if (fsm == nullptr) return nullptr;
  return add_string_cache(fsm->type_name());
}

int component_fsm_is_configured(int cpt_id) {
  AbstractFiniteStateMachine* fsm = component_fsm(cpt_id);
  if (fsm == nullptr) return 0;
  if (fsm->is_configured()) return 1;
  else return 0;
}

int component_fsm_configure(int cpt_id) {
  AbstractFiniteStateMachine* fsm = component_fsm(cpt_id);
  if (fsm == nullptr) return 0;
  if (fsm->configure()) return 1;
  else return 0;
}

void component_fsm_cleanup(int cpt_id) {
  AbstractFiniteStateMachine* fsm = component_fsm(cpt_id);
  if (fsm == nullptr) return;
  fsm->cleanup();
}

AbstractState* component_fsm_state(int cpt_id, int state_id) {
  AbstractFiniteStateMachine* fsm = component_fsm(cpt_id);
  if (fsm == nullptr) return nullptr;
  return fsm->get_state(state_id);
}

int component_fsm_states_number(int cpt_id) {
  AbstractFiniteStateMachine* fsm = component_fsm(cpt_id);
  if (fsm == nullptr) return 0;
  return (int)fsm->get_states_size();
}

int component_fsm_state_is_execution(int cpt_id, int state_id) {
  AbstractState* state = component_fsm_state(cpt_id, state_id);
  if (state == nullptr) return 0;
  return state->is_execution();
}

int component_fsm_state_is_synchronization(int cpt_id, int state_id) {
  AbstractState* state = component_fsm_state(cpt_id, state_id);
  if (state == nullptr) return 0;
  return state->is_synchronization();
}

const char* component_fsm_state_name(int cpt_id, int state_id) {
  AbstractState* state = component_fsm_state(cpt_id, state_id);
  if (state == nullptr) return nullptr;
  return add_string_cache(state->name);
}

long component_fsm_state_clock(int cpt_id, int state_id) {
  AbstractState* state = component_fsm_state(cpt_id, state_id);
  if (state == nullptr) return 0;
  return (long) state->get_clock();
}

int component_fsm_state_next_number(int cpt_id, int state_id) {
  AbstractState* state = component_fsm_state(cpt_id, state_id);
  if (state == nullptr) return 0;
  return (int)state->get_next_size();
}

int component_fsm_state_next(int cpt_id, int state_id, int next_id) {
  AbstractFiniteStateMachine* fsm = component_fsm(cpt_id);
  if (fsm == nullptr) return -1;
  AbstractState* state = fsm->get_state(state_id);
  if (state == nullptr) return -1;
  AbstractState* next = state->get_next_state(next_id);
  if (next == nullptr) return -1;
  return fsm->get_state_index(next);
}


// Component FSM Property
//--------------------------------------------------

AbstractProperty* component_fsm_property(int cpt_id, int prop_id) {
  AbstractFiniteStateMachine* fsm = component_fsm(cpt_id);
  if (fsm == nullptr) return nullptr;
  return fsm->get_property(prop_id);
}

int component_fsm_properties_number(int cpt_id) {
  AbstractFiniteStateMachine* fsm = component_fsm(cpt_id);
  if (fsm == nullptr) return 0;
  return (int)fsm->get_properties_size();
}

const char* component_fsm_property_name(int cpt_id, int prop_id) {
  AbstractProperty* property = component_fsm_property(cpt_id, prop_id);
  if (property == nullptr) return nullptr;
  return add_string_cache(property->name);
}

const char* component_fsm_property_type_name(int cpt_id, int prop_id) {
  AbstractProperty* property = component_fsm_property(cpt_id, prop_id);
  if (property == nullptr) return nullptr;
  return add_string_cache(property->type_name());
}

int component_fsm_property_type(int cpt_id, int prop_id) {
  AbstractProperty* property = component_fsm_property(cpt_id, prop_id);
  if (property == nullptr) return -1;
  switch (property->get_type()) {
    case INTEGRAL: return 0;
    case FLOATING_POINT: return 1;
    case STRING: return 2;
    case OTHER: return 3;
    default: return -1;
  }
}

long component_fsm_property_get_integral_value(int cpt_id, int prop_id) {
  AbstractProperty* property = component_fsm_property(cpt_id, prop_id);
  if (property == nullptr) return 0L;
  if (property->get_type() != INTEGRAL) return 0L;
  return ((AbstractIntegralProperty*)property)->get();
}

int component_fsm_property_set_integral_value(int cpt_id, int prop_id, long value) {
  AbstractProperty* property = component_fsm_property(cpt_id, prop_id);
  if (property == nullptr) return 0;
  if (property->get_type() != INTEGRAL) return 0;
  return ((AbstractIntegralProperty*)property)->set(value);
}

double component_fsm_property_get_floating_value(int cpt_id, int prop_id) {
  AbstractProperty* property = component_fsm_property(cpt_id, prop_id);
  if (property == nullptr) return 0.0;
  if (property->get_type() != FLOATING_POINT) return 0.0;
  return ((AbstractFloatingPointProperty*)property)->get();
}

int component_fsm_property_set_floating_value(int cpt_id, int prop_id, double value) {
  AbstractProperty* property = component_fsm_property(cpt_id, prop_id);
  if (property == nullptr) return 0;
  if (property->get_type() != FLOATING_POINT) return 0;
  return ((AbstractFloatingPointProperty*)property)->set(value);
}

const char* component_fsm_property_get_string_value(int cpt_id, int prop_id) {
  AbstractProperty* property = component_fsm_property(cpt_id, prop_id);
  if (property == nullptr) return nullptr;
  if (property->get_type() != STRING) return nullptr;
  return add_string_cache(((StringProperty*)property)->get_value());
}

int component_fsm_property_set_string_value(int cpt_id, int prop_id, const char* value) {
  AbstractProperty* property = component_fsm_property(cpt_id, prop_id);
  if (property == nullptr) return 0;
  if (property->get_type() != STRING) return 0;
  return ((StringProperty*)property)->set_value(value);
}

// Resource
//--------------------------------------------------

AbstractResource* architecture_resource(int res_id) {
  AbstractDeployer* deployer = AbstractDeployer::instance();
  if (deployer == nullptr) return nullptr;
  Architecture* architecture = deployer->get_architecture();
  if (architecture == nullptr) return nullptr;
  return architecture->get_resource(res_id);
}

const char* resource_name(int res_id) {
  AbstractResource* res = architecture_resource(res_id);
  if (res == nullptr) return nullptr;
  return add_string_cache(res->name());
}

const char* resource_type_name(int res_id) {
  AbstractResource* res = architecture_resource(res_id);
  if (res == nullptr) return nullptr;
  return add_string_cache(res->type_name());
}

int resource_is_configured(int res_id) {
  AbstractResource* res = architecture_resource(res_id);
  if (res == nullptr) return 0;
  return res->is_configured();
}

int resource_configure(int res_id) {
  AbstractResource* res = architecture_resource(res_id);
  if (res == nullptr) return 0;
  return res->configure();
}

void resource_cleanup(int res_id) {
  AbstractResource* res = architecture_resource(res_id);
  if (res == nullptr) return;
  res->cleanup();
}

// Resource Shell
//--------------------------------------------------

Shell* resource_shell(int res_id) {
  AbstractResource* res = architecture_resource(res_id);
  if (res == nullptr) return nullptr;
  return res->get_shell();
}

const char* resource_shell_type_name(int res_id) {
  Shell* shell = resource_shell(res_id);
  return add_string_cache(shell->type_name());
}

int resource_shell_is_configured(int res_id) {
  Shell* shell = resource_shell(res_id);
  return shell->is_configured();
}

int resource_shell_configure(int res_id) {
  Shell* shell = resource_shell(res_id);
  return shell->configure();
}

void resource_shell_cleanup(int res_id) {
  Shell* shell = resource_shell(res_id);
  shell->configure();
}

// Resource Shell Property
//--------------------------------------------------

AbstractProperty* resource_shell_property(int res_id, int prop_id) {
  Shell* shell = resource_shell(res_id);
  if (shell == nullptr) return nullptr;
  return shell->get_property(prop_id);
}

int resource_shell_properties_number(int res_id) {
  Shell* shell = resource_shell(res_id);
  if (shell == nullptr) return 0;
  return (int)shell->get_properties_size();
}

const char* resource_shell_property_name(int res_id, int prop_id) {
  AbstractProperty* property = resource_shell_property(res_id, prop_id);
  if (property == nullptr) return nullptr;
  return add_string_cache(property->name);
}

const char* resource_shell_property_type_name(int res_id, int prop_id) {
  AbstractProperty* property = resource_shell_property(res_id, prop_id);
  if (property == nullptr) return nullptr;
  return add_string_cache(property->type_name());
}

int resource_shell_property_type(int res_id, int prop_id) {
  AbstractProperty* property = resource_shell_property(res_id, prop_id);
  if (property == nullptr) return -1;
  switch (property->get_type()) {
    case INTEGRAL: return 0;
    case FLOATING_POINT: return 1;
    case STRING: return 2;
    case OTHER: return 3;
    default: return -1;
  }
}

long resource_shell_property_get_integral_value(int res_id, int prop_id) {
  AbstractProperty* property = resource_shell_property(res_id, prop_id);
  if (property == nullptr) return 0L;
  if (property->get_type() != INTEGRAL) return 0L;
  return ((AbstractIntegralProperty*)property)->get();
}

int resource_shell_property_set_integral_value(int res_id, int prop_id, long value) {
  AbstractProperty* property = resource_shell_property(res_id, prop_id);
  if (property == nullptr) return 0;
  if (property->get_type() != INTEGRAL) return 0;
  return ((AbstractIntegralProperty*)property)->set(value);
}

double resource_shell_property_get_floating_value(int res_id, int prop_id) {
  AbstractProperty* property = resource_shell_property(res_id, prop_id);
  if (property == nullptr) return 0.0;
  if (property->get_type() != FLOATING_POINT) return 0.0;
  return ((AbstractFloatingPointProperty*)property)->get();
}

int resource_shell_property_set_floating_value(int res_id, int prop_id, double value) {
  AbstractProperty* property = resource_shell_property(res_id, prop_id);
  if (property == nullptr) return 0;
  if (property->get_type() != FLOATING_POINT) return 0;
  return ((AbstractFloatingPointProperty*)property)->set(value);
}

const char* resource_shell_property_get_string_value(int res_id, int prop_id) {
  AbstractProperty* property = resource_shell_property(res_id, prop_id);
  if (property == nullptr) return nullptr;
  if (property->get_type() != STRING) return nullptr;
  return add_string_cache(((StringProperty*)property)->get_value());
}

int resource_shell_property_set_string_value(int res_id, int prop_id, const char* value) {
  AbstractProperty* property = resource_shell_property(res_id, prop_id);
  if (property == nullptr) return 0;
  if (property->get_type() != STRING) return 0;
  return ((StringProperty*)property)->set_value(value);
}

// Resource Shell Port
//--------------------------------------------------

AbstractPort* resource_shell_port(int res_id, int port_id) {
  Shell* shell = resource_shell(res_id);
  if (shell == nullptr) return nullptr;
  return shell->get_port(port_id);
}

int resource_shell_ports_number(int res_id) {
  Shell* shell = resource_shell(res_id);
  if (shell == nullptr) return 0;
  return (int)shell->get_ports_size();
}

const char* resource_shell_port_name(int res_id, int port_id) {
  AbstractPort* port = resource_shell_port(res_id, port_id);
  if (port == nullptr) return nullptr;
  return add_string_cache(port->name);
}

const char* resource_shell_port_type_name(int res_id, int port_id) {
  AbstractPort* port = resource_shell_port(res_id, port_id);
  if (port == nullptr) return nullptr;
  return add_string_cache(port->type_name());
}

int resource_shell_port_type(int res_id, int port_id) {
  AbstractPort* port = resource_shell_port(res_id, port_id);
  if (port == nullptr) return -1;
  switch (port->get_type()) {
    case EVENT: return 0;
    case READ: return 1;
    case WRITE: return 2;
    case CALL: return 3;
    default: return -1;
  }
}

int resource_shell_port_is_connected(int res_id, int port_id) {
  AbstractPort* port = resource_shell_port(res_id, port_id);
  if (port == nullptr) return 0;
  return port->is_connected();
}

int resource_shell_port_connections_number(int res_id, int port_id) {
  AbstractPort* port = resource_shell_port(res_id, port_id);
  if (port == nullptr) return 0;
  return (int) port->connections_size();
}

int resource_shell_port_connection_resource(int res_id, int port_id, int serv_id) {
  AbstractPort* port = resource_shell_port(res_id, port_id);
  if (port == nullptr) return -1;
  Service* service = port->get_connection(serv_id);
  if (service == nullptr) return -2;
  auto res_name = service->get_resource_name();
  AbstractDeployer* deployer = AbstractDeployer::instance();
  Architecture* architecture = deployer->get_architecture();
  return architecture->get_resource_index(res_name);
}

int resource_shell_port_connection_service(int res_id, int port_id, int serv_id) {
  AbstractPort* port = resource_shell_port(res_id, port_id);
  if (port == nullptr) return 0;
  Service* service = port->get_connection(serv_id);
  if (service == nullptr) return -2;
  auto res_name = service->get_resource_name();
  AbstractDeployer* deployer = AbstractDeployer::instance();
  Architecture* architecture = deployer->get_architecture();
  AbstractResource* resource = architecture->get_resource(res_name);
  return resource->get_service_index(service);
}

int resource_shell_port_connect(int src_id, int port_id, int dst_id, int serv_id) {
  AbstractPort* port = resource_shell_port(src_id, port_id);
  if (port == nullptr) return 0;
  Service* service = resource_interface_service(dst_id, serv_id);
  if (service == nullptr) return 0;
  return port->connect_service(service);
}

int resource_shell_port_disconnect(int cpt_id, int port_id) {
  AbstractPort* port = resource_shell_port(cpt_id, port_id);
  if (port == nullptr) return 0;
  return port->disconnect();
}

// Resource Core
//--------------------------------------------------

AbstractCore* resource_core(int res_id) {
  AbstractResource* res = architecture_resource(res_id);
  if (res == nullptr) return nullptr;
  return res->get_core();
}

const char* resource_core_type_name(int res_id) {
  AbstractCore* core = resource_core(res_id);
  if (core == nullptr) return nullptr;
  return add_string_cache(core->type_name());
}

const char* resource_core_to_string(int res_id) {
  AbstractCore* core = resource_core(res_id);
  if (core == nullptr) return nullptr;
  return add_string_cache(core->to_string());
}

int resource_core_is_configured(int res_id) {
  AbstractCore* core = resource_core(res_id);
  if (core == nullptr) return 0;
  if (core->is_configured()) return 1;
  else return 0;
}

int resource_core_configure(int res_id) {
  AbstractCore* core = resource_core(res_id);
  if (core == nullptr) return 0;
  if (core->configure()) return 1;
  else return 0;
}

void resource_core_cleanup(int res_id) {
  AbstractCore* core = resource_core(res_id);
  if (core == nullptr) return;
  core->cleanup();
}

// Resource Core Property
//--------------------------------------------------

AbstractProperty* resource_core_property(int res_id, int prop_id) {
  AbstractCore* core = resource_core(res_id);
  if (core == nullptr) return nullptr;
  return core->get_property(prop_id);
}

int resource_core_properties_number(int res_id) {
  AbstractCore* core = resource_core(res_id);
  if (core == nullptr) return 0;
  return (int)core->get_properties_size();
}

const char* resource_core_property_name(int res_id, int prop_id) {
  AbstractProperty* property = resource_core_property(res_id, prop_id);
  if (property == nullptr) return nullptr;
  return add_string_cache(property->name);
}

const char* resource_core_property_type_name(int res_id, int prop_id) {
  AbstractProperty* property = resource_core_property(res_id, prop_id);
  if (property == nullptr) return nullptr;
  return add_string_cache(property->type_name());
}

int resource_core_property_type(int res_id, int prop_id) {
  AbstractProperty* property = resource_core_property(res_id, prop_id);
  if (property == nullptr) return -1;
  switch (property->get_type()) {
    case INTEGRAL: return 0;
    case FLOATING_POINT: return 1;
    case STRING: return 2;
    case OTHER: return 3;
    default: return -1;
  }
}

long resource_core_property_get_integral_value(int res_id, int prop_id) {
  AbstractProperty* property = resource_core_property(res_id, prop_id);
  if (property == nullptr) return 0L;
  if (property->get_type() != INTEGRAL) return 0L;
  return ((AbstractIntegralProperty*)property)->get();
}

int resource_core_property_set_integral_value(int res_id, int prop_id, long value) {
  AbstractProperty* property = resource_core_property(res_id, prop_id);
  if (property == nullptr) return 0;
  if (property->get_type() != INTEGRAL) return 0;
  return ((AbstractIntegralProperty*)property)->set(value);
}

double resource_core_property_get_floating_value(int res_id, int prop_id) {
  AbstractProperty* property = resource_core_property(res_id, prop_id);
  if (property == nullptr) return 0.0;
  if (property->get_type() != FLOATING_POINT) return 0.0;
  return ((AbstractFloatingPointProperty*)property)->get();
}

int resource_core_property_set_floating_value(int res_id, int prop_id, double value) {
  AbstractProperty* property = resource_core_property(res_id, prop_id);
  if (property == nullptr) return 0;
  if (property->get_type() != FLOATING_POINT) return 0;
  return ((AbstractFloatingPointProperty*)property)->set(value);
}

const char* resource_core_property_get_string_value(int res_id, int prop_id) {
  AbstractProperty* property = resource_core_property(res_id, prop_id);
  if (property == nullptr) return nullptr;
  if (property->get_type() != STRING) return nullptr;
  return add_string_cache(((StringProperty*)property)->get_value());
}

int resource_core_property_set_string_value(int res_id, int prop_id, const char* value) {
  AbstractProperty* property = resource_core_property(res_id, prop_id);
  if (property == nullptr) return 0;
  if (property->get_type() != STRING) return 0;
  return ((StringProperty*)property)->set_value(value);
}

// Resource Interface
//--------------------------------------------------

AbstractInterface* resource_interface(int res_id) {
  AbstractResource* resource = architecture_resource(res_id);
  if (resource == nullptr) return nullptr;
  return resource->get_interface();
}

int resource_interface_is_configured(int res_id) {
  AbstractInterface* interface = resource_interface(res_id);
  if (interface == nullptr) return 0;
  return interface->is_configured();
}

int resource_interface_configure(int res_id) {
  AbstractInterface* interface = resource_interface(res_id);
  if (interface == nullptr) return 0;
  return interface->configure();
}

void resource_interface_cleanup(int res_id) {
  AbstractInterface* interface = resource_interface(res_id);
  if (interface == nullptr) return;
  interface->cleanup();
}

// Resource Interface Services
//--------------------------------------------------

Service* resource_interface_service(int res_id, int serv_id) {
  AbstractInterface* interface = resource_interface(res_id);
  if (interface == nullptr) return nullptr;
  return interface->get_service(serv_id);
}

int resource_interface_services_number(int res_id) {
  AbstractInterface* interface = resource_interface(res_id);
  if (interface == nullptr) return 0;
  return (int)interface->get_services_size();
}

const char* resource_interface_service_name(int res_id, int serv_id) {
  Service* service = resource_interface_service(res_id, serv_id);
  if (service == nullptr) return nullptr;
  return add_string_cache(service->name);
}

const char* resource_interface_service_type_name(int res_id, int serv_id) {
  Service* service = resource_interface_service(res_id, serv_id);
  if (service == nullptr) return nullptr;
  return add_string_cache(service->type_name());
}

int resource_interface_service_type(int res_id, int serv_id) {
  Service* service = resource_interface_service(res_id, serv_id);
  if (service == nullptr) return -1;
  switch (service->get_type()) {
    case EVENT: return 0;
    case READ: return 1;
    case WRITE: return 2;
    case CALL: return 3;
    default: return -1;
  }
}
