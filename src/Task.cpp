/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime/Task.hpp"
#include "mauve/runtime/Component.hpp"
#include "mauve/runtime/AbstractState.hpp"
#include "mauve/runtime/logging/logger.hpp"
#include "mauve/runtime/tracing.hpp"

#include <sstream>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <unistd.h>

namespace mauve {
  namespace runtime {

    // ------------------------- Constructor -------------------------

    Task::Task(AbstractComponent * component)
    : logger { new ComponentLogger(component) }
    , component     { component }
    , status        { CREATED }
    , finish        { false }
    , pt_id         { 0 }
    , time          { 0 }
    , start_barrier { nullptr }
    {
      component->set_task(this);
      // Create Barrier
      pthread_barrierattr_t attr;
      pthread_barrierattr_init(&attr);
      int res = pthread_barrier_init(&barrier, &attr, 2);
      if (res != 0) {
        logger->warn("pthread_barrier_init {} {}}", errno, strerror(errno));
      }
    }

    // ------------------------- Destructor -------------------------

    Task::~Task() {
      pthread_barrier_destroy(&barrier);
      component->set_task(nullptr);
    }

    // ------------------------- Static Method -------------------------

    void *Task::thread_start(void *data) {
      Task *task = (Task *)data;
      task->pt_id = pthread_self();
      task->t_id = (pid_t)syscall(SYS_gettid);

      // ---------- SIG MASK ----------
      sigset_t sig_set;
      sigemptyset(&sig_set);
      sigaddset(&sig_set, TASK_SIG);
      if (sigprocmask(SIG_SETMASK, &sig_set, NULL) != 0) {
        task->logger->warn("sched_setsigmask {} {}", errno, strerror(errno));
      }

      // ---------- Affinity ----------
      int cpu = task->component->get_cpu();
      cpu_set_t set;
      CPU_ZERO(&set);
      CPU_SET(cpu, &set);
      if (sched_setaffinity(0, sizeof(cpu_set_t), &set) != 0) {
        task->logger->warn("sched_setaffinity {} {}", errno, strerror(errno));
      }

      // ---------- Priority ----------
      int priority = task->component->get_priority();
      struct sched_param sp;
      sp.sched_priority = priority;

      if (sched_setscheduler(0, SCHED_FIFO, &sp) != 0) {
        task->logger->warn("sched_setscheduler {} {}", errno, strerror(errno));
      }

      pthread_barrier_wait(&(task->barrier));

      task->run();
      return nullptr;
    }

    // ------------------------- Method -------------------------

    AbstractComponent * Task::get_component() {
      return component;
    }

    pthread_t Task::thread_id() const {
      return pt_id;
    }

    time_ns_t Task::get_time() const {
      return time;
    }

    bool Task::activate() {
      if (!component->is_configured() || status != CREATED) return false;

      // Create Thread
      finish = false;
      if (pthread_create(&pthread_id, NULL, Task::thread_start, this) == -1) {
        logger->error("pthread_create {} {}", errno, strerror(errno));
        return false;
      }

      // Waitting for thread configuration
      pthread_barrier_wait(&barrier);

      // Create Timer
      sev.sigev_notify = SIGEV_THREAD_ID;   // send to thread_id
      sev.sigev_notify_thread_id = t_id;    // Id du thread
      sev.sigev_signo = TASK_SIG;           // signal number
      sev.sigev_value.sival_ptr = &timerid; // Valeur passee au timer
      if (timer_create(MAUVE_CLOCK_ID, &sev, &timerid) == -1) {
        logger->error("timer_create {} {}", errno, strerror(errno));
        pthread_barrier_wait(&barrier);
        stop();
        return false;
      }

      status = ACTIVATED;
      return true;
    }

    bool Task::start(time_ns_t start_time, pthread_barrier_t * start_barrier) {

      if (status != ACTIVATED) return false;

      time = start_time;
      this->start_barrier = start_barrier;
      pthread_barrier_wait(&barrier);

      return true;
    }

    void Task::release() {
      logger->debug("release task");
      // disarm timer
      ns_to_itimerspec(0, 0, &its);
      if (timer_settime(timerid, TIMER_ABSTIME, &its, NULL) == -1) {
        logger->warn("timer_settime {} {}", errno, strerror(errno));
      }
      // send signal
      pthread_kill(pt_id, TASK_SIG);
    }

    bool Task::stop() {
      if (status != RUNNING) return false;

      finish = true;
      release();
      pthread_join(pt_id, NULL);
      status = CREATED;
      return true;
    }

    void Task::run() {
      pthread_barrier_wait(&barrier);
      if (start_barrier != nullptr) {
        pthread_barrier_wait(start_barrier);
      }
      status = RUNNING;

      sigset_t sig_set;
      sigemptyset(&sig_set);
      sigaddset(&sig_set, TASK_SIG);

      while (true) {

        if (finish) {
          status = CREATED;
          pthread_exit(NULL);
          return; // Useless
        }

        AbstractState * state = component->current_state();

        if (state->is_execution()) {
          trace_execution_begin(component, state);
          component->run();
          trace_execution_end(component, state);
        }
        else if (state->is_synchronization()) {
          trace_synchronization_begin(component, state);
          // Next
          time_ns_t next = time + state->get_clock();
          ns_to_itimerspec(0, next, &its);
          if (timer_settime(timerid, TIMER_ABSTIME, &its, NULL) == -1) {
            logger->warn("timer_settime {} {}", errno, strerror(errno));
          }
          // logger->trace("Wait state {}", state->name);
          sigwaitinfo(&sig_set, NULL);
          time += component->current_state()->get_clock();
          component->run();
          trace_synchronization_end(component, state);
        }
      }
    }

    std::string Task::to_string() const {
      std::ostringstream os;
      os << "task " << component->name() << " [" << get_status() << "]";
      return os.str();
    }

  }
} /* namepsace mauve */
