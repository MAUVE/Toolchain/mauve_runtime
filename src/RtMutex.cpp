/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime/RtMutex.hpp"

namespace mauve {
  namespace runtime {

  RtMutex::RtMutex() {
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_setprotocol(&attr, PTHREAD_PRIO_INHERIT); // PIP
    pthread_mutex_init(&mutex, &attr);
  }

  RtMutex::~RtMutex() noexcept {
    pthread_mutex_destroy(&mutex);
  }

  void RtMutex::lock() {
    pthread_mutex_lock(&mutex);
  }

  void RtMutex::unlock() {
    pthread_mutex_unlock(&mutex);
  }

}}
