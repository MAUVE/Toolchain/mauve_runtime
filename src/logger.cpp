/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include <sstream>
#include <locale>
#include <algorithm>

#include <yaml-cpp/yaml.h>

#include "mauve/runtime/Deployer.hpp"
#include "mauve/runtime/Component.hpp"
#include "mauve/runtime/logging/logger.hpp"
#include "mauve/runtime/logging/factory.hpp"

#include "spdlog/sinks/null_sink.h"

namespace mauve {
  namespace runtime {

//------------------------------- AbstractLogger --------------------------//

std::map<std::string,
  std::vector< std::shared_ptr<spdlog::logger> > > AbstractLogger::loggers;

std::shared_ptr<spdlog::logger> AbstractLogger::default_logger_ = nullptr;

AbstractLogger::AbstractLogger()
{}

AbstractLogger::~AbstractLogger()
{}

void AbstractLogger::initialize() {
  YAML::Node args;
  args["queue_size"] = 8192;
  args["default"]["level"] = "info";
  args["default"]["type"] = "stdout";
  initialize(args);
}

void AbstractLogger::initialize(const std::string& s) {
  YAML::Node root = YAML::Load(s);
  initialize(root);
}

void AbstractLogger::initialize(std::istream& is) {
  YAML::Node root = YAML::Load(is);
  initialize(root);
}

void AbstractLogger::initialize(const YAML::Node& root) {
  // get queue size
  size_t queue_size = 8192;
  if (root["queue_size"])
    queue_size = (size_t)root["queue_size"].as<int>();
  spdlog::set_async_mode(queue_size,
    spdlog::async_overflow_policy::discard_log_msg);
  // default logger
  std::string type = "stderr";
  std::string level = "warn";
  YAML::Node args;
  if (root["default"]) {
    args = root["default"];
    if (args["level"]) level = args["level"].as<std::string>();
    if (args["type"]) type = args["type"].as<std::string>();
  }
  try {
    default_logger_ = mk_logger("runtime", type, args);
  } catch (spdlog::spdlog_ex& e) {
    default_logger_ = spdlog::get("runtime");
  }
  default_logger_->set_level(level_enum(level));
  // custom loggers
  for (auto cat: root) {
    std::string name = cat.first.as<std::string>();
    if (name == "default") continue;
    // user defined loggers
    for (auto l: cat.second ) {
      // For each logger of a category
      type = l["type"].as<std::string>();
      level = l["level"].as<std::string>();
      // logger label
      std::stringstream label;
      label << name << loggers[name].size();
      auto logger = mk_logger(label.str(), type, l);
      logger->set_level(level_enum(level));
      if (loggers.find(name) == std::end(loggers))
        loggers[name] = std::vector< std::shared_ptr<spdlog::logger> >();
      loggers[name].push_back(logger);
    }
  }
}

void AbstractLogger::clear() {
  // reset
  spdlog::drop_all();
  AbstractLogger::default_logger_ = nullptr;
  AbstractLogger::loggers.clear();
}

AbstractLogger* AbstractLogger::get_logger(const std::string& name) {
  return new CategoryLogger(name);
}

spdlog::level::level_enum AbstractLogger::level_enum(const std::string& lvl) {
  std::array<std::string ,7> levels;
  std::copy(std::begin(spdlog::level::level_names),
    std::end(spdlog::level::level_names), std::begin(levels));
  auto level_it = std::find(levels.begin(), levels.end(), lvl);
  auto level_i = std::distance(levels.begin(), level_it);
  return static_cast<spdlog::level::level_enum>(level_i);
}

//------------------------------- CategoryLogger --------------------------//

CategoryLogger::CategoryLogger(const std::string& name)
: AbstractLogger()
, name_ { name }
{}

std::string CategoryLogger::prepend(const char* fmt){
  std::stringstream ss;
  ss << "[" << name() << "] " << fmt;
  return ss.str();
}

std::string CategoryLogger::name() { return name_; }

//------------------------------- ComponentLogger --------------------------//

ComponentLogger::ComponentLogger(AbstractComponent* component)
: AbstractLogger()
, component_ { component }
{}

std::string ComponentLogger::prepend(const char* fmt) {
  std::stringstream ss;
  // Component time
  Task* t = AbstractDeployer::instance()->get_task(component_);
  if (t) ss << "(" << t->get_time() << ") ";
  else ss << "(0) ";
  // Component name
  ss << "[" << name();
  // Component status
  ss  << " (" << status() << ")] ";
  // Original message
  ss << fmt;
  return ss.str();
}

std::string ComponentLogger::name() { return component_->name(); }

std::string ComponentLogger::status() {
  if (component_->is_running()) return "R";
  else if (component_->is_activated()) return "A";
  else if (component_->is_configured()) return "C";
  else if (component_->is_empty()) return "E";
  else return "U";
}

//------------------------------- ResourceLogger --------------------------//

ResourceLogger::ResourceLogger(AbstractResource* resource)
: AbstractLogger()
, resource_ { resource }
{}

std::string ResourceLogger::prepend(const char* fmt) {
  std::stringstream ss;
  // Resource name
  ss << "[" << name();
  // Resource status
  ss  << " (" << status() << ")] ";
  // Original message
  ss << fmt;
  return ss.str();
}

std::string ResourceLogger::name() { return resource_->name(); }

std::string ResourceLogger::status() {
  if (resource_->is_configured()) return "C";
  else if (resource_->is_empty()) return "E";
  else return "C";
}

//------------------------------- DeployerLogger --------------------------//

DeployerLogger::DeployerLogger()
: AbstractLogger()
{}

std::string DeployerLogger::prepend(const char* fmt) {
  std::stringstream ss;
  ss << "(" << AbstractDeployer::instance()->now() << ") ";
  ss << "[" << name() << "] " << fmt;
  return ss.str();
}

std::string DeployerLogger::name() { return "runtime"; }

}}
