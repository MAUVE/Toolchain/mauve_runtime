/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/runtime/AbstractState.hpp"
#include "mauve/runtime/AbstractFiniteStateMachine.hpp"

namespace mauve {
  namespace runtime {

  // ------------------------- Constructor -------------------------

  AbstractState::AbstractState(AbstractFiniteStateMachine* container, std::string const & name)
  : name      { name }
  , container { container }
  {}

  // ------------------------- Destructor -------------------------

  AbstractState::~AbstractState() noexcept {}

  // ------------------------- Function -------------------------

  std::ostream & operator<<(std::ostream & out, AbstractState const & state) {
    out << state.to_string();
    return out;
  }

}} /* namespace mauve */
