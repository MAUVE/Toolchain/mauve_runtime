/*
 * Copyright 2018 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */

#include <iostream>

#include "mauve/runtime/PropertyTree.hpp"
#include "mauve/runtime/HasProperty.hpp"
#include "mauve/runtime/WithLogger.hpp"
#include "mauve/runtime/AbstractComponent.hpp"
#include "mauve/runtime/AbstractResource.hpp"
#include "mauve/runtime/Shell.hpp"
#include "mauve/runtime/AbstractCore.hpp"
#include "mauve/runtime/Architecture.hpp"

namespace mauve {
  namespace runtime {

YAML::Node PropertyTree::property_tree_ = YAML::Load("{}");

void PropertyTree::add(std::istream& is) {
  add(YAML::Load(is));
}

void PropertyTree::add(const std::string& s) {
  add(YAML::Load(s));
}

void PropertyTree::clear() {
  PropertyTree::property_tree_ = YAML::Node();
}

void PropertyTree::add(const YAML::Node& n) {
  //std::cout << "add " << n << " to " << PropertyTree::property_tree_ << std::endl;
  PropertyTree::property_tree_ = merge(PropertyTree::property_tree_, n);
}

const YAML::Node & PropertyTree::cnode(const YAML::Node &n) {
  return n;
}

YAML::Node PropertyTree::merge(const YAML::Node& a, const YAML::Node&b) {
  if (!b.IsMap()) {
    // If b is not a map, merge result is b, unless b is null
    //std::cout << b << " is not a map => return " << (b.IsNull() ? a : b) << std::endl;
    return b.IsNull() ? a : b;
  }
  if (!a.IsMap()) {
    // If a is not a map, merge result is b
    //std::cout << a << " is not a map => return " << b << std::endl;
    return b;
  }
  if (!b.size()) {
    //std::cout << b << " is empty" << std::endl;
    // If a is a map, and b is an empty map, return a
    return a;
  }
  // Create a new map 'c' with the same mappings as a, merged with b
  auto c = YAML::Node(YAML::NodeType::Map);
  for (auto n : a) {
    //std::cout << "loop on 'a': merge " << n.second << " in " << n.first << std::endl;
    if (n.first.IsScalar()) {
      const std::string & key = n.first.Scalar();
      auto t = YAML::Node(cnode(b)[key]);
      if (t) {
        c[n.first] = merge(n.second, t);
        continue;
      }
    }
    c[n.first] = n.second;
  }
  // Add the mappings from 'b' not already in 'c'
  for (auto n : b) {
    if (!n.first.IsScalar() || !cnode(c)[n.first.Scalar()]) {
      //std::cout << "loop on 'b': merge " << n.second << " in " << n.first << std::endl;
      c[n.first] = n.second;
    }
  }
  return c;
}

const YAML::Node& PropertyTree::get() {
  return PropertyTree::property_tree_;
}

template <>
bool PropertyTree::configure(HasProperty* s, const YAML::Node& properties, WithLogger* l) {
  if (!properties.IsMap()) {
    l->logger().error("properties are not given as a map; cannot configure");
    return false;
  }
  for (auto n: properties) {
    const std::string & key = n.first.Scalar();
    AbstractProperty* p = s->get_property(key);
    if (!p) {
      l->logger().warn("property '{}' given in config file does not exist; ignoring", key);
      continue;
    } else {
      StringProperty* sp = dynamic_cast<StringProperty*>(p);
      if (sp) {
        std::string v = n.second.as<std::string>();
        l->logger().debug("string property '{}' set to {}", sp->name, v);
        sp->set_value(v);
        continue;
      }
      AbstractIntegralProperty* ip = dynamic_cast<AbstractIntegralProperty*>(p);
      if (ip) {
        ip->set(n.second.as<int>());
        l->logger().debug("integral property '{}' set to {}", ip->name, ip->get());
        continue;
      }
      AbstractFloatingPointProperty* fp = dynamic_cast<AbstractFloatingPointProperty*>(p);
      if (fp) {
        fp->set(n.second.as<double>());
        l->logger().debug("floating point property '{}' set to {}", fp->name, fp->get());
        continue;
      }
      // else
      l->logger().warn("type not supported for property '{}'; ignoring", key);
    }
  }
  return true;
}

template <>
bool PropertyTree::configure(AbstractComponent* c, const YAML::Node& properties, WithLogger* l) {
  if (!properties.IsMap()) {
    l->logger().warn("properties are not given as a map; cannot configure");
    return false;
  }
  if (properties["shell"]) {
    l->logger().debug("configuring Shell properties of {}", c->name());
    bool r = PropertyTree::configure<HasProperty>(c->get_shell(), properties["shell"], c);
    if (!r) return false;
  }
  if (properties["core"]) {
    l->logger().debug("configuring Core properties of {}", c->name());
    bool r = PropertyTree::configure<HasProperty>(c->get_core(), properties["core"], c);
    if (!r) return false;
  }
  if (properties["fsm"]) {
    l->logger().debug("configuring FSM properties of {}", c->name());
    bool r = PropertyTree::configure<HasProperty>(c->get_fsm(), properties["fsm"], c);
    if (!r) return false;
  }
  if (properties["priority"]) {
    int prio = properties["priority"].as<int>();
    if (c->set_priority(prio))
      l->logger().debug("configuring priority of {} to {}", c->name(), prio);
    else
      l->logger().error("configuring priority of {} to {}", c->name(), prio);
  }
  if (properties["cpu"]) {
    int cpu = properties["cpu"].as<int>();
    if (c->set_cpu(cpu))
      l->logger().debug("configuring cpu of {} to {}", c->name(), cpu);
    else
      l->logger().error("configuring cpu of {} to {}", c->name(), cpu);
  }
  return true;
}

template <>
bool PropertyTree::configure(AbstractResource* c, const YAML::Node& properties, WithLogger* l) {
  if (!properties.IsMap()) {
    l->logger().warn("properties are not given as a map; cannot configure");
    return false;
  }
  if (properties["shell"]) {
    l->logger().debug("configuring Shell properties of {}", c->name());
    bool r = PropertyTree::configure<HasProperty>(c->get_shell(), properties["shell"], l);
    if (!r) return false;
  }
  if (properties["core"]) {
    l->logger().debug("configuring Core properties of {}", c->name());
    bool r = PropertyTree::configure<HasProperty>(c->get_core(), properties["core"], l);
    if (!r) return false;
  }
  if (properties["interface"]) {
    l->logger().debug("configuring Interface properties of {}", c->name());
    bool r = PropertyTree::configure<HasProperty>(c->get_interface(), properties["interface"], l);
    if (!r) return false;
  }
  return true;
}

template <>
bool PropertyTree::configure(Architecture* a, const YAML::Node& properties, WithLogger* l) {
  if (!properties.IsMap()) {
    l->logger().warn("properties are not given as a map; cannot configure");
    return false;
  }
  for (auto n: properties) {
    const std::string & key = n.first.Scalar();
    AbstractComponent* comp = a->get_component(key);
    if (comp) {
      l->logger().debug("configuring Component properties of {}", comp->name());
      bool r = PropertyTree::configure(comp, n.second, comp);
      if (!r) return false;
    } else {
      AbstractResource* res = a->get_resource(key);
      if (res) {
        l->logger().debug("configuring Resource properties of {}", res->name());
        bool r = PropertyTree::configure(res, n.second, /*res*/l);
        if (!r) return false;
      } else
        l->logger().warn("Element {} unknown", key);
    }
  }
  return true;
}

}} // ns
