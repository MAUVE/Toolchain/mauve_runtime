/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */

#include "mauve/runtime/tracing.hpp"
#include "mauve/runtime.hpp"

#define TRACEPOINT_DEFINE
#define TRACEPOINT_PROBE_DYNAMIC_LINKAGE
#include <mauve/tracing/mauve-tp.h>

namespace mauve {
  namespace runtime {

    void trace_component_start(AbstractComponent* c) {
      tracepoint(mauve, component_start,
        c->name().c_str(),
        c->get_time());
    }

    void trace_execution_begin(AbstractComponent* c, AbstractState* s) {
      tracepoint(mauve, state_execution_begin,
        c->name().c_str(),
        c->get_time(),
        s->name.c_str());
    }

    void trace_execution_end(AbstractComponent* c, AbstractState* s) {
      tracepoint(mauve, state_execution_end,
        c->name().c_str(),
        c->get_time(),
        s->name.c_str());
    }

    void trace_synchronization_begin(AbstractComponent* c, AbstractState* s) {
      tracepoint(mauve, state_synchronization_begin,
        c->name().c_str(),
        c->get_time(),
        s->name.c_str());
    }

    void trace_synchronization_end(AbstractComponent* c, AbstractState* s) {
      tracepoint(mauve, state_synchronization_end,
        c->name().c_str(),
        c->get_time(),
        s->name.c_str());
    }

    void trace_service_begin(const std::string& r, const std::string& s) {
      tracepoint(mauve, service_execution_begin,
        r.c_str(),
        s.c_str());
    }

    void trace_service_end(const std::string& r, const std::string& s) {
      tracepoint(mauve, service_execution_end,
        r.c_str(),
        s.c_str());
    }

}}
