/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Runtime project.
 *
 * MAUVE Runtime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Runtime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include <ctime>
//#include <iomanip>

#include <yaml-cpp/yaml.h>
#include <spdlog/sinks/null_sink.h>

#include "mauve/runtime/logging/factory.hpp"
#include "mauve/runtime/logging/logger.hpp"
#include "mauve/runtime/logging/mauve_formatter.hpp"
#include "mauve/runtime/logging/rosjson.hpp"

namespace mauve {
  namespace runtime {

std::string default_file_name() {
  std::stringstream s;
  s << "/tmp/mauve-";
  auto t = std::time(nullptr);
  auto tm = *std::localtime(&t);
  char str[20];
  std::strftime(str, sizeof(str), "%Y-%m-%d-%H-%M-%S", &tm);
  s << str << ".log";
  return s.str();
}

std::shared_ptr<spdlog::logger> mk_logger(const std::string& label,
  const std::string& type, const YAML::Node& args)
{
  AbstractLogger* log = new CategoryLogger("runtime");
  std::shared_ptr<spdlog::logger> logger;

  if (type == "rotating_file") {
    std::string base_filename = ( args["filename"] ?
      args["filename"].as<std::string>() : default_file_name() );
    long max_size = ( args["max_size"] ?
      args["max_size"].as<long>() : (1048576*5) );
    int max_files = ( args["max_files"] ?
      args["max_files"].as<int>() : 10 );
    log->debug("creating logger {} with base filename {}, max size {}, max files {}",
      type, base_filename, max_size, max_files);
    logger = spdlog::rotating_logger_mt(label, base_filename, (size_t)max_size, (size_t)max_files);
  }
  else if (type == "daily_file") {
    std::string base_filename = ( args["filename"] ?
      args["filename"].as<std::string>() : default_file_name() );
    int hour = ( args["hour"] ?  args["hour"].as<int>() : (0) );
    int minutes = ( args["minutes"] ? args["minutes"].as<int>() : 0 );
    log->debug("creating logger {} with base filename {}, rotation hour {}:{}",
      type, base_filename, hour, minutes);
    logger = spdlog::daily_logger_mt(label, base_filename, hour, minutes);
  }
  else if (type == "simple_file" || type == "file") {
    std::string base_filename = ( args["filename"] ?
      args["filename"].as<std::string>() : default_file_name() );
    log->debug("creating logger simple_file with filename {}", base_filename);
    logger = spdlog::basic_logger_mt(label, base_filename);
  }
  else if (type == "stdout" || type == "stdout_color") {
    log->debug("creating logger stdout_color");
    logger = spdlog::stdout_color_mt(label);
  }
  else if (type == "stderr" || type == "stderr_color") {
    log->debug("creating logger stderr_color");
    logger = spdlog::stderr_color_mt(label);
  }
  else if (type == "stdout_nocolor") {
    log->debug("creating logger stdout");
    logger = spdlog::stdout_logger_mt(label);
  }
  else if (type == "stderr_nocolor") {
    log->debug("creating logger stderr");
    logger = spdlog::stderr_logger_mt(label);
  }
  else if (type == "null") {
    log->debug("creating null logger");
    auto null_sink = std::make_shared<spdlog::sinks::null_sink_mt> ();
    logger = std::make_shared<spdlog::logger> (label, null_sink);
    spdlog::register_logger(logger);
  }
  else if (type == "rosjson") {
    log->debug("creating ROS-Json logger");
    std::string server_ip = ( args["ip"] ?
      args["ip"].as<std::string>() : "127.0.0.1" );
    int server_port = ( args["port"] ?
      args["port"].as<int>() : 9090 );
    auto sink = std::make_shared<rosjson_sink>(server_ip, server_port);
    logger = std::make_shared<spdlog::logger> (label, sink);
    spdlog::register_logger(logger);
  }
  else {
    log->warn("Unknown logger or wrong parameters in {} {}", type, args);
    log->debug("creating null logger");
    auto null_sink = std::make_shared<spdlog::sinks::null_sink_mt> ();
    logger = std::make_shared<spdlog::logger> (label, null_sink);
    spdlog::register_logger(logger);
  }
  logger->set_formatter(std::make_shared<mauve_formatter>());
  return logger;
}

}}
