'''
# Copyright 2017 ONERA
#
# This file is part of the MAUVE Runtime project.
#
# MAUVE Runtime is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# MAUVE Runtime is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
'''
from libcpp.string cimport string
from libcpp.vector cimport vector
from cython.operator cimport dereference as deref, preincrement as inc

# ------------------------- Logger -------------------------

cdef extern from "mauve/runtime/logging/logger.hpp":
  cdef cppclass C_Logger "mauve::runtime::AbstractLogger":
    @staticmethod
    void initialize(string)
    @staticmethod
    void clear()

def initialize_logger(s):
    C_Logger.initialize(s)

def initialize_logger_from_file(filename):
    f = open(filename)
    s = f.read()
    f.close()
    initialize_logger(s)

def clear_logger():
    C_Logger.clear()

# ------------------------- Deployer -------------------------

cdef extern from "mauve/runtime/Deployer.hpp":
  cdef cppclass C_Deployer "mauve::runtime::AbstractDeployer":
    C_Architecture* get_architecture()
    C_Task* get_task(C_Component*)
    long now()
    long get_time()
    bint create_tasks()
    bint activate()
    bint start()
    bint start_deployer()
    void stop()
    void clear_tasks()
    vector[string] manager_actions()
    bint manager_apply(string)
    @staticmethod
    C_Deployer* instance()

cdef class Deployer(object):
  cdef C_Deployer* ptr
  cdef object __architecture

  def __cinit__(self):
    self.ptr = <C_Deployer*> C_Deployer.instance()
    self.__architecture = Architecture(Py_Architecture(<long>self.ptr.get_architecture()))

  def __dealloc__(self):
    del self.ptr

  def __repr__(self):
    return "deployer"

  @property
  def architecture(self):
    return self.__architecture

  @property
  def manager(self):
    return self.__manager

  @property
  def now(self):
    return self.ptr.now()

  @property
  def clock(self):
    return self.ptr.get_time()

  def create_tasks(self):
    return self.ptr.create_tasks()

  def activate(self):
    return self.ptr.activate()

  def start(self):
    return self.ptr.start()

  def start_deployer(self):
    return self.ptr.start_deployer()

  def stop(self):
    self.ptr.stop()

  def clear_tasks(self):
    self.ptr.clear_tasks()

  def task(self, Component cpt):
    return Task(<long>self.ptr.get_task(cpt.ptr))

  def manager_apply(self, name):
    cdef string s_name = name.encode("utf-8")
    return self.ptr.manager_apply(s_name)

  def manager_actions(self):
    cdef vector[string].iterator it
    v = self.ptr.manager_actions()
    list = []
    it = v.begin()
    while (it != v.end()):
      # name = <bytes>deref(it).decode()
      list.append(deref(it))
      inc(it)
    return list


deployer = None

def load_deployer(path):
  global deployer
  import ctypes
  lib = ctypes.CDLL(path)
  lib.mk_python_deployer()
  deployer = Deployer()

# ------------------------- Task -------------------------

cdef extern from "mauve/runtime/Task.hpp":
  cdef cppclass C_Task "mauve::runtime::Task":
    C_Component* get_component()
    long get_time()
    bint activate()
    bint start(long)
    bint stop()
    string to_string()


cdef class Task(object):
  cdef C_Task* ptr

  def __cinit__(self, long task):
    self.ptr = <C_Task*> task

  def __repr__(self):
    return <bytes> self.ptr.to_string().decode()

  @property
  def component(self):
    return Component(<long>self.ptr.get_component())

  def activate(self):
    return self.ptr.activate()

  def start(self, clock):
    return self.ptr.start(clock)

  def stop(self):
    return self.ptr.stop()

# ------------------------- Architecture -------------------------

cdef extern from "mauve/runtime/Architecture.hpp":
  cdef cppclass C_Architecture "mauve::runtime::Architecture":
    string type_name()
    bint is_configured()
    bint configure()
    void cleanup()
    vector[C_Component*] get_components()
    vector[C_Resource*]  get_resources()

cdef class Py_Architecture(object):
  cdef C_Architecture* ptr

  def __cinit__(self, long archi):
    self.ptr = <C_Architecture*> archi

  def __repr__(self):
    return "Py_Architecture<{}>".format(self.type_name())

  def type_name(self):
    return <bytes> self.ptr.type_name().decode()

  def is_configured(self):
    return self.ptr.is_configured()

  def configure(self):
    return self.ptr.configure()

  def cleanup(self):
    return self.ptr.cleanup()

  def get_components(self):
    cdef vector[C_Component*].iterator it
    v = self.ptr.get_components()
    list = []
    it = v.begin()
    while (it != v.end()):
      list.append(Component(<long>(<void*>deref(it))))
      inc(it)
    return list

  def get_resources(self):
    cdef vector[C_Resource*].iterator it
    v = self.ptr.get_resources()
    list = []
    it = v.begin()
    while (it != v.end()):
      list.append(Resource(<long>(<void*>deref(it))))
      inc(it)
    return list

class Architecture(object):

  def __init__(self, py_archi):
    self.__py_archi = py_archi
    self.__components = []
    self.__resources = []
    if self.__py_archi.is_configured():
        self.__get_components_and_resources()

  def __repr__(self):
    return "architecture {}".format(self.type)

  def __str__(self):
    return self.type

  @property
  def ptr(self):
    return self.__py_archi.ptr

  @property
  def type(self):
    return self.__py_archi.type_name()

  @property
  def is_configured(self):
    return self.__py_archi.is_configured()

  @property
  def components(self):
    return self.__components

  @property
  def resources(self):
    return self.__resources

  def __get_components_and_resources(self):
    self.__components = self.__py_archi.get_components()
    for cpt in self.__components:
      setattr(self, cpt.name, cpt)
    self.__resources = self.__py_archi.get_resources()
    for res in self.__resources:
      setattr(self, res.name, res)

  def configure(self):
    configured = self.__py_archi.configure()
    if configured:
        self.__get_components_and_resources()
    return configured

  def cleanup(self):
    for cpt in self.__components:
      delattr(self, cpt.name)
    self.__components = []
    for res in self.__resources:
      delattr(self, res.name)
    self.__resources = []
    self.__py_archi.cleanup()

  def display(self):
    str = repr(self) + " ["
    if not self.is_configured: str += "not "
    str += "configured]"
    print(str)
    for cpt in self.components:
      str = "  " + repr(cpt) + " ["
      if not cpt.is_configured: str += "not "
      str += "configured]"
      print(str)
    for res in self.resources:
      str = "  " + repr(res)
      print(str)

# ------------------------- Component -------------------------

cdef extern from "mauve/runtime/Component.hpp":
  cdef cppclass C_Component "mauve::runtime::AbstractComponent":
    string name()
    string type_name()
    bint is_empty()
    bint is_configured()
    bint is_activated()
    bint is_running()
    bint configure()
    void cleanup()
    C_Shell* get_shell()
    bint configure_shell()
    void cleanup_shell()
    C_Core* get_core()
    bint configure_core()
    void cleanup_core()
    C_FSM* get_fsm()
    bint configure_fsm()
    void cleanup_fsm()
    C_State current_state()
    bint step()
    int get_priority()
    bint set_priority(int)
    int get_cpu()
    bint set_cpu(int)


cdef class Component(object):
  cdef C_Component* ptr

  def __cinit__(self, long cpt):
    self.ptr = <C_Component*> cpt

  def __repr__(self):
    return self.type + " " + self.name

  def __str__(self):
    return self.name

  @property
  def name(self):
    return <bytes> self.ptr.name().decode()

  @property
  def type(self):
    return <bytes> self.ptr.type_name().decode()

  @property
  def is_empty(self):
    return self.ptr.is_empty()

  @property
  def is_configured(self):
    return self.ptr.is_configured()

  @property
  def is_activated(self):
    return self.ptr.is_activated()

  @property
  def is_running(self):
    return self.ptr.is_running()

  def configure(self):
    return self.ptr.configure()

  def cleanup(self):
    return self.ptr.cleanup()

  @property
  def shell(self):
    return Shell(Py_Shell(<long>self.ptr.get_shell()))

  def configure_shell(self):
    return self.ptr.configure_shell()

  def cleanup_shell(self):
    return self.ptr.cleanup_shell()

  @property
  def core(self):
    return Core(Py_Core(<long>self.ptr.get_core()))

  def configure_core(self):
    return self.ptr.configure_core()

  def cleanup_core(self):
    return self.ptr.cleanup_core()

  @property
  def fsm(self):
    return FSM(Py_FSM(<long>self.ptr.get_fsm()))

  def configure_fsm(self):
    return self.ptr.configure_fsm()

  def cleanup_fsm(self):
    return self.ptr.cleanup_fsm()

  @property
  def current_state(self):
    return State(<long>self.ptr.current_state())

  def step(self):
    return self.ptr.step()

  @property
  def priority(self):
    return self.ptr.get_priority()

  @priority.setter
  def priority(self, priority):
    self.ptr.set_priority(priority)

  @property
  def cpu(self):
    return self.ptr.get_cpu()

  @cpu.setter
  def cpu(self, cpu):
    self.ptr.set_cpu(cpu)

  def display(self):
    str = repr(self) + " ["
    if self.is_empty: str += "E"
    elif self.is_configured: str += "C"
    elif self.is_activated: str += "A"
    elif self.is_running: str += "R"
    else: str += "N"
    str += "]"
    print(str)
    str = "  " + repr(self.shell) + " ["
    if self.shell.is_configured: str += "C"
    else: str += "N"
    str += "]"
    print(str)
    str = "  " + repr(self.core) + " ["
    if self.core.is_configured: str += "C"
    else: str += "N"
    str += "]"
    print(str)
    str = "  " + repr(self.fsm) + " ["
    if self.fsm.is_configured: str += "C"
    else: str += "N"
    str += "]"
    print(str)

# ------------------------- Shell -------------------------

cdef extern from "mauve/runtime/Shell.hpp":
  cdef cppclass C_Shell "mauve::runtime::Shell":
    string type_name()
    bint is_configured()
    vector[C_Property*] get_properties()
    vector[C_Port*] get_ports()

cdef class Py_Shell(object):
  cdef C_Shell* ptr

  def __cinit__(self, long shell):
    self.ptr = <C_Shell*> shell

  def __repr__(self):
    return "Py_Shell {}".format(self.type_name())

  def type_name(self):
    return <bytes> self.ptr.type_name().decode()

  def is_configured(self):
    return self.ptr.is_configured()

  def get_properties(self):
    cdef vector[C_Property*].iterator it
    v = self.ptr.get_properties()
    list = []
    it = v.begin()
    while (it != v.end()):
      prop = Property(<long>(<void*>deref(it)))
      if prop.property_type == 0:
        p = IntegralProperty(<long>(<void*>deref(it)))
        list.append(p)
      elif prop.property_type == 1:
          p = FloatingPointProperty(<long>(<void*>deref(it)))
          list.append(p)
      elif prop.property_type == 2:
          p = StringProperty(<long>(<void*>deref(it)))
          list.append(p)
      else:
        list.append(prop)
      inc(it)
    return list

  def get_ports(self):
    cdef vector[C_Port*].iterator it
    v = self.ptr.get_ports()
    list = []
    it = v.begin()
    while (it != v.end()):
      port = Port(<long>(<void*>deref(it)))
      list.append(port)
      inc(it)
    return list

class Shell(object):

  def __init__(self, py_shell):
    self.__py_shell = py_shell
    self.__properties = self.__py_shell.get_properties()
    self.__ports = self.__py_shell.get_ports()
    for prop in self.__properties:
      setattr(self, prop.name, prop)
    for port in self.__ports:
      setattr(self, port.name, port)

  def __repr__(self):
    return "shell {}".format(self.type)

  def __str__(self):
    return self.type

  @property
  def type(self):
    return self.__py_shell.type_name()

  @property
  def is_configured(self):
    return self.__py_shell.is_configured()

  @property
  def properties(self):
    return self.__properties

  @property
  def ports(self):
    return self.__ports

  def display(self):
    str = repr(self) + " ["
    if not self.is_configured: str += "not "
    str += "configured]"
    print(str)
    for prop in self.properties:
      str = "  " + repr(prop)
      print(str)
    for port in self.ports:
      str = "  " + repr(port)
      print(str)

# ------------------------- Core -------------------------

cdef extern from "mauve/runtime/AbstractCore.hpp":
  cdef cppclass C_Core "mauve::runtime::AbstractCore":
    string type_name()
    string shell_type_name()
    bint is_configured()
    vector[C_Property*] get_properties()

cdef class Py_Core(object):
  cdef C_Core* ptr

  def __cinit__(self, long core):
    self.ptr = <C_Core*> core

  def __repr__(self):
    return "core {}<{}>".format(self.type, self.shell_type)

  def type_name(self):
    return <bytes> self.ptr.type_name().decode()

  def shell_type(self):
    return <bytes> self.ptr.shell_type_name().decode()

  def is_configured(self):
    return self.ptr.is_configured()

  def get_properties(self):
    cdef vector[C_Property*].iterator it
    v = self.ptr.get_properties()
    list = []
    it = v.begin()
    while (it != v.end()):
      prop = Property(<long>(<void*>deref(it)))
      if prop.property_type == 0:
        p = IntegralProperty(<long>(<void*>deref(it)))
        list.append(p)
      elif prop.property_type == 1:
          p = FloatingPointProperty(<long>(<void*>deref(it)))
          list.append(p)
      elif prop.property_type == 2:
          p = StringProperty(<long>(<void*>deref(it)))
          list.append(p)
      else:
        list.append(prop)
      inc(it)
    return list

class Core(object):

  def __init__(self, py_core):
    self.__py_core = py_core
    self.__properties = self.__py_shell.get_properties()
    for port in self.__ports:
      setattr(self, port.name, port)

  def __repr__(self):
    return "core {}".format(self.type)

  def __str__(self):
    return self.type

  @property
  def type(self):
   return self.__py_core.type_name()

  @property
  def shell_type(self):
    return self.__py_core.shell_type()

  @property
  def is_configured(self):
    return self.__py_core.is_configured()

  @property
  def properties(self):
    return self.__properties

  def display(self):
    str = repr(self) + " ["
    if not self.is_configured: str += "not "
    str += "configured]"
    print(str)


# ------------------------- Finite State Machine -------------------------

cdef extern from "mauve/runtime/AbstractFiniteStateMachine.hpp":
  cdef cppclass C_FSM "mauve::runtime::AbstractFiniteStateMachine":
    string type_name()
    string shell_type_name()
    string core_type_name()
    bint is_configured()
    vector[C_Property*] get_properties()

cdef class Py_FSM(object):
  cdef C_FSM* ptr

  def __cinit__(self, long fsm):
    self.ptr = <C_FSM*> fsm

  def type_name(self):
    return <bytes> self.ptr.type_name().decode()

  def shell_type(self):
    return <bytes> self.ptr.shell_type_name().decode()

  def core_type(self):
    return <bytes> self.ptr.core_type_name().decode()

  def is_configured(self):
    return self.ptr.is_configured()

  def get_properties(self):
    cdef vector[C_Property*].iterator it
    v = self.ptr.get_properties()
    list = []
    it = v.begin()
    while (it != v.end()):
      prop = Property(<long>(<void*>deref(it)))
      if prop.property_type == 0:
        p = IntegralProperty(<long>(<void*>deref(it)))
        list.append(p)
      elif prop.property_type == 1:
          p = FloatingPointProperty(<long>(<void*>deref(it)))
          list.append(p)
      elif prop.property_type == 2:
          p = StringProperty(<long>(<void*>deref(it)))
          list.append(p)
      else:
        list.append(prop)
      inc(it)
    return list

class FSM(object):

  def __init__(self, py_fsm):
    self.__py_fsm = py_fsm
    self.__properties = self.__py_shell.get_properties()
    for prop in self.__properties:
      setattr(self, prop.name, prop)

  def __repr__(self):
    return "fsm {}<{},{}>".format(self.type_name, self.shell_type, self.core_type)

  def __str__(self):
    return self.type_name

  @property
  def type(self):
    return self.__py_fsm.type_name()

  @property
  def is_configured(self):
    return self.__py_fsm.is_configured()

  @property
  def properties(self):
    return self.__properties

  def display(self):
    str = repr(self) + " ["
    if not self.is_configured: str += "not "
    str += "configured]"
    print(str)

# ------------------------- State -------------------------

cdef extern from "mauve/runtime/AbstractState.hpp":
  cdef cppclass C_State "mauve::runtime::AbstractState":
    string name
    bint is_execution()
    bint is_synchronization()
    int get_clock()
    bint set_clock(int)
    string to_string()

cdef class State(object):
  cdef C_State* ptr

  def __cinit__(self, long state):
    self.ptr = <C_State*> state

  def __repr__(self):
    return self.ptr.to_string().decode()

  def __str__(self):
    return self.name

  @property
  def name(self):
    return self.ptr.name.decode()

  @property
  def is_execution(self):
    return self.ptr.is_execution()

  @property
  def is_synchronization(self):
    return self.ptr.is_synchronization()

  @property
  def clock(self):
    return self.ptr.get_clock()

  @clock.setter
  def clock(self, clock):
    self.ptr.set_clock(clock)

# ------------------------- Property -------------------------

cdef extern from "mauve/runtime/Property.hpp":
  cdef cppclass C_Property "mauve::runtime::AbstractProperty":
    string name
    string type_name()
    int get_type()
  cdef cppclass C_IntegralProperty "mauve::runtime::AbstractIntegralProperty":
    string name
    string type_name()
    long get()
    void set(long)
  cdef cppclass C_FloatingPointProperty "mauve::runtime::AbstractFloatingPointProperty":
    string name
    string type_name()
    double get()
    void set(double)
  cdef cppclass C_StringProperty "mauve::runtime::StringProperty":
    string name
    string type_name()
    string get_value()
    void set_value(string)

# ---------- Property ----------

cdef class Property(object):
  cdef C_Property* ptr

  def __cinit__(self, long prop):
    self.ptr = <C_Property*> prop

  def __repr__(self):
    return "Property<" + self.type + "> " + self.name

  def __str__(self):
    return self.name

  @property
  def name(self):
    return self.ptr.name.decode()

  @property
  def type(self):
    return self.ptr.type_name().decode()

  @property
  def property_type(self):
    return self.ptr.get_type()

# ---------- Integral Property ----------

cdef class IntegralProperty(object):
  cdef C_IntegralProperty* ptr

  def __cinit__(self, long prop):
    self.ptr = <C_IntegralProperty*> prop

  def __repr__(self):
    return "Property<" + self.type + "> " + self.name + " = " + str(self.value)

  def __str__(self):
    return self.name

  @property
  def name(self):
    return self.ptr.name.decode()

  @property
  def type(self):
    return self.ptr.type_name().decode()

  def __get_value(self):
    return self.ptr.get()

  def __set_value(self, long value):
    self.ptr.set(value)

  value = property(__get_value, __set_value)

# ---------- Floating Point Property ----------

cdef class FloatingPointProperty(object):
  cdef C_FloatingPointProperty* ptr

  def __cinit__(self, long prop):
    self.ptr = <C_FloatingPointProperty*> prop

  def __repr__(self):
    return "Property<" + self.type + "> " + self.name + " = " + str(self.value)

  def __str__(self):
    return self.name

  @property
  def name(self):
    return self.ptr.name.decode()

  @property
  def type(self):
    return self.ptr.type_name().decode()

  def __get_value(self):
    return self.ptr.get()

  def __set_value(self, double value):
    self.ptr.set(value)

  value = property(__get_value, __set_value)

# ---------- String Property ----------

cdef class StringProperty(object):
  cdef C_StringProperty* ptr

  def __cinit__(self, long prop):
    self.ptr = <C_StringProperty*> prop

  def __repr__(self):
    return "Property<" + self.type + "> " + self.name + " = " + str(self.value)

  def __str__(self):
    return self.name

  @property
  def name(self):
    return self.ptr.name.decode()

  @property
  def type(self):
    return self.ptr.type_name().decode()

  def __get_value(self):
    return self.ptr.get_value().decode()

  def __set_value(self, value):
    self.ptr.set_value(value.encode("utf-8"))

  value = property(__get_value, __set_value)

# ------------------------- Port -------------------------

cdef extern from "mauve/runtime/AbstractPort.hpp":
  cdef cppclass C_Port "mauve::runtime::AbstractPort":
    string name
    string type_name()
    bint is_connected()
    bint connect_service(C_Service *)
    void disconnect()

cdef class Port(object):
  cdef C_Port* ptr

  def __cinit__(self, long port):
    self.ptr = <C_Port*> port

  def __repr__(self):
    return "{} {}".format(self.type, self.name)

  def __str__(self):
    return self.name

  @property
  def name(self):
    return self.ptr.name.decode()

  @property
  def type(self):
    return self.ptr.type_name().decode()

  @property
  def is_connected(self):
    return self.ptr.is_connected()

  def connect(self, Service service):
    return self.ptr.connect_service(service.ptr)

# ------------------------- Service -------------------------

cdef extern from "mauve/runtime/Service.hpp":
  cdef cppclass C_Service "mauve::runtime::Service":
    string name
    string type_name()

cdef class Service(object):
  cdef C_Service* ptr

  def __cinit__(self, long service):
    self.ptr = <C_Service*> service

  def __repr__(self):
    return "service {}: {}".format(self.name, self.type)

  @property
  def name(self):
    return self.ptr.name.decode()

  @property
  def type(self):
    return self.ptr.type_name().decode()

# ------------------------- Resource -------------------------

cdef extern from "mauve/runtime/AbstractResource.hpp":
  cdef cppclass C_Resource "mauve::runtime::AbstractResource":
    string name()
    string type_name()

cdef class Resource(object):
  cdef C_Resource* ptr

  def __cinit__(self, long resource):
    self.ptr = <C_Resource*> resource

  def __repr__(self):
    return "resource {} {}".format(self.name, self.type)

  @property
  def name(self):
    return self.ptr.name().decode()

  @property
  def type(self):
    return self.ptr.type_name().decode()
