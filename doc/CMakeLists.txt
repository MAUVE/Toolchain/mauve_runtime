#####
# Copyright 2017 ONERA
#
# This file is part of the MAUVE Runtime project.
#
# MAUVE Runtime is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# MAUVE Runtime is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
#####
find_package ( Doxygen )
if ( NOT DOXYGEN_FOUND )
    message(STATUS "Doxygen is needed to build the documentation. Please install it to activate doc")
else ()

    string(TOUPPER ${PROJECT_NAME} PROJECT_NAME_UPPER)

    set(doxyfile_in ${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in)
    set(doxyfile ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile)

    configure_file(${doxyfile_in} ${doxyfile} @ONLY)

    add_custom_target (${PROJECT_NAME}_doc ALL
        ${DOXYGEN_EXECUTABLE} ${doxyfile}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    )

endif ()
